import {Platform, StatusBar, StyleSheet} from 'react-native';
export const viewStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  loader: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: '100%',
    height: '90%',
    zIndex: 1,
  },
  statusBarPadding: {
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight + 10 : 10,
  },
});
export const textStyle = StyleSheet.create({
  title: {
    color: '#0A1F44',
    fontSize: 34,
    fontFamily: 'Avenir-Heavy',
    lineHeight: 41,
    fontWeight: '900',
    marginTop: 0,
    marginBottom: 20,
    marginHorizontal: 30,
  },
  title_2: {
    color: '#0A1F44',
    fontSize: 12,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
  button: {
    fontFamily: 'Avenir',
    color: '#FFFFFF',
    fontWeight: '900',
    fontSize: 15,
    textAlign: 'center',
  },
});
export const imageStyle = StyleSheet.create({
  navigationGoBack: {
    width: 18,
    height: 16,
    marginTop: Platform.OS === 'android' ? StatusBar.currentHeight + 20 : 20,
    margin: 20,
  },
  user_50x50: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
  },
});

export const buttonStyle = StyleSheet.create({
  submit: {
    marginVertical: 40,
    paddingVertical: 15,
    width: '75%',
    alignSelf: 'center',
    backgroundColor: '#EF4236',
    borderRadius: 8,
  },
  submitMarginAuto: {
    marginTop: 'auto',
    marginBottom: 40,
    paddingVertical: 15,
    width: '75%',
    alignSelf: 'center',
    backgroundColor: '#EF4236',
    borderRadius: 8,
  },
});

export const radioStyle = StyleSheet.create({
  buttonWrapStyle: {
    marginLeft: 40,
    marginTop: 25,
  },
  labelStyle: {
    fontSize: 16,
    color: '#0A1F44',
    fontFamily: 'Avenir',
    fontWeight: '500',
  },
  labelWrapStyle: {
    marginTop: 25,
    marginLeft: 10,
  },
});
