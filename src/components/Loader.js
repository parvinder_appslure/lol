import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import {viewStyle} from '../style/style';

const Loader = () => (
  <View style={viewStyle.loader}>
    <ActivityIndicator size="large" color="#EF4236" />
  </View>
);

export default Loader;
