import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  Text,
  StatusBar,
} from 'react-native';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {GoogleSigninJson} from '../backend/Config';

import bgImage from '../assets/wel-bg.png';
import logoImage from '../assets/wel-logo.png';
import fbImage from '../assets/facebook.png';
import googleImage from '../assets/google.png';

const Login = ({navigation}) => {
  const navLoginAccount = () => navigation.navigate('LoginAccount');
  const navCreateAccount = () => navigation.navigate('CreateAccount');
  const loginWithFacebook = () => {
    LoginManager.setLoginBehavior('web_only');
    LoginManager.logInWithPermissions(['public_profile']).then(
      (result) => {
        if (result.isCancelled) {
          alert('Login was cancelled');
        } else {
          alert(
            'Login was successful with permissions: ' +
              result.grantedPermissions.toString(),
          );
          AccessToken.getCurrentAccessToken().then((data) => {
            const {accessToken} = data;
            console.log(data);
            // fetch(
            //   'https://graph.facebook.com/v2.5/me?fields=email,name,picture.type(large),friends&access_token=' +
            //     data.accessToken,
            // )
            //   .then((response) => response.json())
            //   .then((json) => {
            //     console.log(JSON.stringify(json, null, 2));
            //   });
          });
        }
      },
      (error) => {
        alert('Login failed with error: ' + error);
      },
    );
  };
  const loginWithGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const isSignedIn = await GoogleSignin.isSignedIn();
      console.log(isSignedIn);
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
      return;
      console.log(statusCodes.SIGN_IN_CANCELLED);
      console.log(statusCodes.IN_PROGRESS);
      console.log(statusCodes.PLAY_SERVICES_NOT_AVAILABLE);
      console.log(JSON.stringify(userInfo, null, 2));
    } catch (error) {
      console.log(JSON.stringify(error, null, 2));
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  useEffect(() => {
    GoogleSignin.configure(GoogleSigninJson);
  }, []);

  return (
    <>
      <StatusBar
        barStyle="dark-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <ImageBackground source={bgImage} style={styles.container}>
        <Image source={logoImage} style={styles.logoImage} />
        <View style={styles.mainView}>
          <TouchableOpacity
            style={styles.loginButton}
            activeOpacity={0.5}
            onPress={navLoginAccount}>
            <Text style={styles.loginText}>Log In</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.fbButton}
            activeOpacity={0.5}
            onPress={loginWithFacebook}>
            <Text style={styles.fbText}>
              <Image source={fbImage} style={styles.logoIcon}></Image>
              {'   '}Connect with Facebook
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.googleButton}
            activeOpacity={0.5}
            onPress={loginWithGoogle}>
            <Text style={styles.googleText}>
              <Image source={googleImage} style={styles.logoIcon}></Image>
              {'   '}Connect with Google
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.view_1} onPress={navCreateAccount}>
            <Text style={styles.textSignUp}>
              Don't have an account? Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoImage: {
    marginTop: '20%',
    width: 180,
    height: 102,
    alignSelf: 'center',
  },
  mainView: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  loginButton: {
    marginTop: 10,
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 33,
    marginRight: 33,
    backgroundColor: '#EF4236',
    borderRadius: 8,
  },
  loginText: {
    fontFamily: 'Avenir',
    color: '#FFFFFF',
    fontWeight: '900',
    fontSize: 15,
    textAlign: 'center',
  },
  fbButton: {
    marginVertical: 16,
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 33,
    marginRight: 33,
    backgroundColor: '#2672CB',
    borderRadius: 8,
  },
  logoIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  fbText: {
    fontFamily: 'Avenir',
    color: '#FFFFFF',
    fontWeight: '900',
    fontSize: 15,
    textAlign: 'center',
  },
  googleButton: {
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 33,
    marginRight: 33,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
  },
  googleText: {
    fontFamily: 'Avenir',
    color: '#F14336',
    fontWeight: '900',
    fontSize: 15,
    textAlign: 'center',
  },
  textSignUp: {
    alignSelf: 'center',
    marginTop: 35,
    marginBottom: 42,
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    color: '#FFFFFF',
  },
});

export default Login;
