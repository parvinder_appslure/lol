import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';
import {textInPrice} from '../backend/Api';
import {imageStyle, textStyle, viewStyle} from '../style/style';

const DonationHistory = ({navigation}) => {
  return (
    <SafeAreaView style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/Close.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>Donation History</Text>

      <View style={styles.view_1}>
        <Text style={styles.text_1}>Transaction Id"12345</Text>
        <View style={styles.view_2}>
          <Image
            style={styles.image_1}
            source={require('../assets/donation-icon.png')}
          />
          <View style={styles.view_3}>
            <Text style={styles.text_2}>LOL APP</Text>
            <Text style={styles.text_3}>17 Aug, 07:36 pm</Text>
          </View>
          <Text style={styles.text_4}>{textInPrice('3000')}</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  image_1: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },
  view_1: {
    flex: 0,
    margin: 16,
    padding: 16,
    borderRadius: 6,
    borderColor: '#97979766',
    borderWidth: 1,
  },
  view_2: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 14,
    borderColor: '#9797971a',
    borderTopWidth: 1,
  },
  view_3: {
    flex: 0,
    justifyContent: 'space-around',
    marginLeft: 9,
  },
  text_1: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 13,
    color: '#1e2432e6',
    paddingBottom: 9,
  },
  text_2: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 14,
    lineHeight: 18,
    color: '#1E2432',
    paddingBottom: 4,
  },
  text_3: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 12,
    color: '#1e243280',
  },
  text_4: {
    fontFamily: 'Ping Fang SC',
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 22,
    color: '#EF4236',
    marginLeft: 'auto',
  },
});
export default DonationHistory;
