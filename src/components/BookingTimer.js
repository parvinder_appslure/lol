import React, {useEffect, useState, useRef} from 'react';
import {
  Text,
  SafeAreaView,
  Animated,
  StyleSheet,
  Image,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {CountdownCircleTimer} from 'react-native-countdown-circle-timer';
import TextTicker from 'react-native-text-ticker';
import {useSelector} from 'react-redux';
import {
  timeFormate_mmss,
  BookingCheckApi,
  AsyncStorageSetTime,
  CancelBookingUserApi,
} from '../backend/Api';
import {viewStyle} from '../style/style';
const BookingTimer = ({navigation, route}) => {
  const {location} = useSelector((state) => state);
  const [state, _setState] = useState({
    ...route.params,
    timer: 600,
    show: 0,
    duration: 600,
  });
  const showRef = useRef(0);
  const setState = (data) => {
    showRef.current = data.show;
    _setState(data);
  };
  const updateShow = (show) => setState({...state, show});
  const onCompleteHandler = () => {
    if (state.show === 0) updateShow(2);
  };

  const messageView = (source, h1, h2) => (
    <View style={styles.messageContainer}>
      <Image source={source} style={styles.image} />
      <Text style={styles.text_1}>{h1}</Text>
      <Text style={styles.text_2}>{h2}</Text>
    </View>
  );
  useEffect(() => {
    AsyncStorageSetTime(Date.now().toString());
    let timer;
    const callback = async () => {
      // console.log(showRef.current);
      if (showRef.current === 3) return;
      const body = {
        user_id: state.user_id,
        id: state.id,
        type: 'request',
      };
      const {status = false, booking_status} = await BookingCheckApi(body);
      if (status) {
        console.log('booking_status', booking_status);
        if (booking_status === 0 && showRef.current === 0) {
          timer = setTimeout(callback, 10000);
        }
        if ([1, 2].includes(booking_status)) {
          updateShow(booking_status);
        }
      } else {
        updateShow(2);
      }
    };
    callback();
    return () => {
      console.log('unmount BookingTimer');
      clearTimeout(timer);
    };
  }, []);

  useEffect(() => {
    console.log('flag', state.show);
    if ([1, 2, 3].includes(state.show)) {
      setTimeout(
        () =>
          navigation.reset({
            index: 0,
            routes: [{name: 'MyDrawer'}],
          }),
        2000,
      );
    }
  }, [state.show]);

  const cancelRequestHandler = () => {
    const cancelRequest = async () => {
      const {status = false} = await CancelBookingUserApi({
        booking_id: route.params.id,
      });
      if (status) {
        updateShow(3);
      } else {
        alert('Something Went Wrong');
      }
    };

    Alert.alert(
      'Booking Cancel Request',
      `Do you want to cancel this booking request.`,
      [
        {
          text: 'No',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Yes', onPress: cancelRequest},
      ],
      {cancelable: false},
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity
          onPress={() =>
            navigation.reset({
              index: 0,
              routes: [{name: 'MyDrawer'}],
            })
          }
          style={styles.headerTouch}>
          <Image
            style={styles.headerImage}
            source={require('../assets/Close.png')}
          />
        </TouchableOpacity>
        <Text style={styles.title}>{`Booking Details`}</Text>
      </View>
      {state.show === 0 && (
        <>
          <View style={styles.coord_container}>
            <View style={styles.coordsSelectView}>
              <View style={styles.CSV_1}>
                <Image
                  source={require('../assets/origin2.png')}
                  style={styles.imageOrigin}
                />
                <Image
                  source={require('../assets/coordsLine.png')}
                  style={styles.imageCordsLine}
                />
                <Image
                  source={require('../assets/destination2.png')}
                  style={styles.imageDestination}
                />
              </View>
              <View style={styles.CSV_2}>
                <View style={styles.textTickerView}>
                  <Text style={styles.textLocation}>Pickup Location</Text>
                  <TextTicker
                    style={styles.textTicker}
                    duration={8000}
                    loop
                    bounce
                    repeatSpacer={50}
                    marqueeDelay={3000}>
                    {location.pickup}
                  </TextTicker>
                </View>
                <View style={styles.horizontalLine}></View>
                <View style={styles.textTickerView}>
                  <Text style={styles.textLocation}>Drop Location</Text>
                  <TextTicker
                    style={styles.textTicker}
                    duration={8000}
                    loop
                    bounce
                    repeatSpacer={50}
                    marqueeDelay={3000}>
                    {location.drop}
                  </TextTicker>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.counterContainer}>
            <CountdownCircleTimer
              isPlaying
              duration={state.duration}
              colors={[
                ['#F7B801', 0.4],
                ['#EF4236', 0.4],
                ['#A30000', 0.2],
              ]}
              onComplete={onCompleteHandler}>
              {({remainingTime, animatedColor}) => (
                <Animated.Text style={{color: animatedColor}}>
                  {timeFormate_mmss(remainingTime)}
                </Animated.Text>
              )}
            </CountdownCircleTimer>
            <Text style={styles.conformText}>{`Confirming Your\nBooking`}</Text>

            <TouchableOpacity
              style={styles.dv_cancelTouch}
              onPress={cancelRequestHandler}>
              <Text style={styles.dv_cancelText}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </>
      )}
      {state.show === 1 &&
        messageView(
          require('../assets/tick.png'),
          'Booking Successful',
          'Your booking has been confirmed',
        )}
      {state.show === 2 &&
        messageView(
          require('../assets/cancel.png'),
          'Booking Failed',
          'Your booking has been Failed.',
        )}
      {state.show === 3 &&
        messageView(
          require('../assets/tick.png'),
          'Booking Request Cancel',
          'Your booking request has been cancelled',
        )}
    </SafeAreaView>
  );
};

export default BookingTimer;

const styles = StyleSheet.create({
  container: {
    ...viewStyle.container,
    ...viewStyle.statusBarPadding,
  },
  header: {
    justifyContent: 'center',
    marginBottom: 30,
  },
  headerTouch: {
    position: 'absolute',
    paddingHorizontal: 20,
  },
  headerImage: {
    width: 18,
    height: 16,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '500',
    fontSize: 30,
    color: '#0A1F44',
    alignSelf: 'center',
  },
  counterContainer: {
    marginTop: 'auto',
    marginBottom: '25%',
    alignItems: 'center',
  },
  conformText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '500',
    fontSize: 20,
    color: '#EF4236',
    marginVertical: 30,
    textAlign: 'center',
  },
  dv_cancelTouch: {
    backgroundColor: '#F50000',
    padding: 5,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  dv_cancelText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#FFFFFF',
  },
  messageContainer: {
    marginTop: '35%',
    alignItems: 'center',
  },
  image: {
    height: 100,
    width: 100,
    resizeMode: 'cover',
  },
  text_1: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 22,
    color: '#0A1F44',
    marginVertical: 16,
  },
  text_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    color: '#000000',
  },
  coord_container: {
    margin: 20,
    marginTop: '15%',
    padding: 10,
    paddingVertical: 20,
    borderRadius: 15,
    alignItems: 'center',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  coordsSelectView: {
    alignSelf: 'center',
    borderRadius: 16,
    backgroundColor: '#ffffff',
    flexDirection: 'row',
  },
  CSV_1: {
    flex: 0.2,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  imageOrigin: {
    width: 14,
    height: 18,
    resizeMode: 'contain',
  },
  imageCordsLine: {
    width: 1.5,
    resizeMode: 'cover',
    marginVertical: 2,
  },
  imageDestination: {
    width: 14,
    height: 18,
    resizeMode: 'contain',
  },
  CSV_2: {
    flex: 1,
    justifyContent: 'center',
  },
  textTickerView: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 16,
  },
  textLocation: {
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 12,
    color: '#ABAFBD',
    marginBottom: 2,
  },
  textTicker: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#242E42',
  },
  horizontalLine: {
    width: '100%',
    height: 1,
    backgroundColor: '#EFEFEF',
  },
});
