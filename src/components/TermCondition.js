import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';
import {imageStyle, viewStyle, textStyle} from '../style/style';

const TermCondition = ({navigation}) => {
  return (
    <SafeAreaView style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/Close.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>Terms and Conditions</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({});
export default TermCondition;
