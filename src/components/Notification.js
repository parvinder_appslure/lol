import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';
import {imageStyle, viewStyle, textStyle} from '../style/style';

const Notification = ({navigation}) => {
  return (
    <SafeAreaView style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/Close.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>Notification</Text>
      <View style={styles.view_1}>
        <Image
          style={imageStyle.user_50x50}
          source={require('../assets/user.png')}
        />
        <View style={styles.view_2}>
          <Text style={styles.text_1}>Ajay Dutt liked your Post</Text>
          <Text style={styles.text_2}>2 hours ago</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  view_1: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: 'coral',
  },
  view_2: {
    flex: 1,
    justifyContent: 'center',
    minHeight: 60,
    // backgroundColor: 'azure',
    borderBottomWidth: 1,
    borderColor: '#F1F2F4',
  },
  text_1: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 15,
    lineHeight: 20,
    color: '#000000',
  },
  text_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 20,
    color: '#00000066',
  },
});
export default Notification;
