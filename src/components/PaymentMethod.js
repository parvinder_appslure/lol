import React, {useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {textInPrice} from '../backend/Api';
import {
  buttonStyle,
  imageStyle,
  radioStyle,
  textStyle,
  viewStyle,
} from '../style/style';

const PaymentMethod = ({navigation}) => {
  const [paymentState, setPaymentState] = useState(0);
  const radio_props = [
    {label: 'UPI', value: 0},
    {label: 'Debit/Credit Card', value: 1},
    {label: 'Wallet', value: 2},
  ];
  const onRadioPress = (value) => setPaymentState(value);
  const showBackButton = () => (
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <Image
        style={imageStyle.navigationGoBack}
        source={require('../assets/Close.png')}
      />
    </TouchableOpacity>
  );
  const showTitle = () => <Text style={textStyle.title}>Payment Method</Text>;

  const showPay = () => (
    <TouchableOpacity
      style={buttonStyle.submitMarginAuto}
      activeOpacity={0.5}
      onPress={() => navigation.navigate('PaymentSuccess')}>
      <Text style={textStyle.button}>PAY</Text>
    </TouchableOpacity>
  );
  return (
    <SafeAreaView style={viewStyle.container}>
      {showBackButton()}
      {showTitle()}
      <Text style={styles.amountText}>{textInPrice('3000')}</Text>
      <Text style={styles.amountText_2}>AMOUNT TO PAY</Text>
      <View style={styles.paymentView}>
        <Text style={styles.paymentText}>Select Payment Method</Text>
      </View>

      <RadioForm formHorizontal={false} animation={true}>
        {radio_props.map((obj, i) => (
          <RadioButton labelHorizontal={true} key={i}>
            <RadioButtonInput
              obj={obj}
              index={i}
              isSelected={paymentState === i}
              onPress={onRadioPress}
              borderWidth={1}
              buttonInnerColor={'#EF4236'}
              buttonOuterColor={paymentState === i ? '#EF4236' : '#0A1F44'}
              buttonSize={10}
              buttonOuterSize={20}
              buttonStyle={{}}
              buttonWrapStyle={radioStyle.buttonWrapStyle}
            />
            <RadioButtonLabel
              obj={obj}
              index={i}
              labelHorizontal={true}
              onPress={onRadioPress}
              labelStyle={radioStyle.labelStyle}
              labelWrapStyle={radioStyle.labelWrapStyle}
            />
          </RadioButton>
        ))}
      </RadioForm>

      {showPay()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  amountText: {
    fontFamily: 'Ping Fang SC',
    fontWeight: '600',
    fontSize: 50,
    color: '#0A1F44',
    alignSelf: 'center',
    marginTop: 10,
  },
  amountText_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 13,
    color: '#000000',
    opacity: 0.5,
    alignSelf: 'center',
  },
  paymentView: {
    marginVertical: 30,
    paddingVertical: 12,
    paddingHorizontal: 20,
    backgroundColor: '#F7F7F7',
  },
  paymentText: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 16,
    color: '#0A1F44',
  },
});

export default PaymentMethod;
