import React, {useState} from 'react';
import {View, TouchableOpacity, Image, Text, StyleSheet} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {textInPrice} from '../backend/Api';
import {
  buttonStyle,
  imageStyle,
  radioStyle,
  textStyle,
  viewStyle,
} from '../style/style';
const BookingComplete = ({navigation}) => {
  var radio_props = [
    {label: 'UPI', value: 0},
    {label: 'Debit/Credit Card', value: 1},
    {label: 'Wallet', value: 2},
    {label: 'Cash', value: 3},
  ];

  const onRadioPress = (value) => setBooking(value);
  const [booking, setBooking] = useState(0);
  return (
    <View style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/Close.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>Booking Complete</Text>
      <Text style={styles.textAmount}>{textInPrice('2000')}</Text>
      <Text style={styles.text_1}>AMOUNT TO PAY</Text>

      <View style={styles.view_1}>
        <View style={styles.view_2}>
          <Image
            style={styles.imageIcon}
            source={require('../assets/list.png')}
          />
          <Text style={styles.text_2}>Distance</Text>
          <Text style={styles.text_3}>25 KM</Text>
        </View>
        <View style={styles.vrt} />
        <View style={styles.view_2}>
          <Image
            style={styles.imageIcon}
            source={require('../assets/list.png')}
          />
          <Text style={styles.text_2}>Distance</Text>
          <Text style={styles.text_3}>10 min</Text>
        </View>
      </View>
      <Text style={styles.text_4}>Select Payment Method</Text>

      <RadioForm formHorizontal={false} animation={true}>
        {radio_props.map((obj, i) => (
          <RadioButton labelHorizontal={true} key={i}>
            <RadioButtonInput
              obj={obj}
              index={i}
              isSelected={booking === i}
              onPress={onRadioPress}
              borderWidth={1}
              buttonInnerColor={'#EF4236'}
              buttonOuterColor={booking === i ? '#EF4236' : '#0A1F44'}
              buttonSize={10}
              buttonOuterSize={20}
              buttonStyle={{}}
              buttonWrapStyle={radioStyle.buttonWrapStyle}
            />
            <RadioButtonLabel
              obj={obj}
              index={i}
              labelHorizontal={true}
              onPress={onRadioPress}
              labelStyle={radioStyle.labelStyle}
              labelWrapStyle={radioStyle.labelWrapStyle}
            />
          </RadioButton>
        ))}
      </RadioForm>

      <TouchableOpacity
        style={buttonStyle.submitMarginAuto}
        activeOpacity={0.5}
        onPress={() => navigation.navigate('')}>
        <Text style={textStyle.button}>Pay</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  imageIcon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
  },
  textAmount: {
    fontFamily: 'Ping Fang SC',
    fontWeight: '600',
    fontSize: 50,
    color: '#0A1F44',
    alignSelf: 'center',
  },
  text_1: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 13,
    lineHeight: 25,
    color: '#00000080',
    alignSelf: 'center',
  },
  text_2: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 12,
    lineHeight: 16,
    color: '#8A94A6',
    marginTop: 8,
  },
  text_3: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 20,
    lineHeight: 28,
    color: '#0A1F44',
  },
  text_4: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 16,
    color: '#0A1F44',
    paddingVertical: 12,
    paddingHorizontal: 25,
    backgroundColor: '#F7F7F7',
    marginVertical: 24,
  },
  view_1: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 18,
  },
  vrt: {
    backgroundColor: '#97979766',
    height: '100%',
    width: 1,
    marginHorizontal: 28,
  },
  view_2: {
    flex: 0,
    alignItems: 'center',
  },
});
export default BookingComplete;
