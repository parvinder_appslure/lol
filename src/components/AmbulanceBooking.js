import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  SafeAreaView,
  View,
  FlatList,
  Dimensions,
  Modal,
} from 'react-native';
import * as actions from '../redux/actions';
import {
  LatitudeDelta,
  LongitudeDelta,
  GeocoderLocation,
  textInPrice,
  AmbulanceCategoryApi,
  EstimateApi,
} from '../backend/Api';

import MapView, {Marker} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import TextTicker from 'react-native-text-ticker';

import {GOOGLE_MAPS_APIKEY} from '../backend/Config';
import originImage from '../assets/origin.png';
import destinationImage from '../assets/destination.png';
import {imageStyle, radioStyle, textStyle, viewStyle} from '../style/style';
const {width, height} = Dimensions.get('window');
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {useDispatch, useSelector} from 'react-redux';

const AmbulanceBooking = ({route, navigation}) => {
  const dispatch = useDispatch();
  const {coords, user} = useSelector((state) => state);
  const [region, setRegion] = useState({
    latitude: coords.latitude,
    longitude: coords.longitude,
    latitudeDelta: LatitudeDelta,
    longitudeDelta: LongitudeDelta(),
  });
  const [distance, setDistance] = useState(0);
  const [duration, setDuration] = useState(0);
  const [originCoords, setOriginCoords] = useState({latitude: 0, longitude: 0});
  const [destinationCoords, setDestinationCoords] = useState({
    latitude: 0,
    longitude: 0,
  });
  const [location, setLocation] = useState({pickup: '', drop: ''});
  const [ambulanceState, setAmbulanceState] = useState({
    list: [],
    selected: {
      id: '',
    },
    wallet_amount: 0,
  });
  const [stateModal, setStateModal] = useState({
    type: 0,
    props: [
      {label: 'Covid Ambulance', value: 0},
      {label: 'Non Covid Ambulance', value: 1},
    ],
    show: false,
    navigate: '',
  });
  const [walletState, setWalletState] = useState({
    show: false,
    amount: 0,
  });
  const [bookingFor, setBookingFor] = useState({
    show: true,
    type: 'self',
    mobile: '',
  });

  const showTitle = () => (
    <>
      <Text style={[textStyle.title, {marginBottom: 0}]}>
        Ambulance Booking
      </Text>
      <View style={styles.title_view_55}>
        <Text style={textStyle.title_2}>{`Duration: ${Math.round(
          duration,
        )} mins approx`}</Text>
        <Text style={textStyle.title_2}>{`Distance: ${Math.round(
          distance,
        )} kms approx`}</Text>
      </View>
    </>
  );
  const showBackButton = () => (
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <Image
        style={imageStyle.navigationGoBack}
        source={require('../assets/Close.png')}></Image>
    </TouchableOpacity>
  );
  const showMapView = () => (
    <MapView
      style={styles.map}
      initialRegion={region}
      showsUserLocation={true}
      showsMyLocationButton={false}
      pitchEnabled={true}
      rotateEnabled={true}
      zoomEnabled={true}
      scrollEnabled={true}
      onRegionChangeComplete={(region) => setRegion(region)}
      onPress={(event) => console.log(event.nativeEvent.coordinate)}>
      {showMarker(originCoords, originImage)}
      {showMarker(destinationCoords, destinationImage)}
      {showDirection()}
    </MapView>
  );
  const showMarker = (coordinate, source) =>
    coordinate.latitude !== 0 &&
    coordinate.longitude !== 0 && (
      <Marker coordinate={coordinate}>
        <Image source={source} style={styles.markerIcon} />
      </Marker>
    );
  const showDirection = () =>
    ![
      originCoords.latitude,
      originCoords.longitude,
      destinationCoords.latitude,
      destinationCoords.latitude,
    ].includes(0) && (
      <MapViewDirections
        origin={originCoords}
        destination={destinationCoords}
        strokeWidth={3}
        strokeColor="#F76B1C"
        apikey={GOOGLE_MAPS_APIKEY}
        onReady={(result) => {
          setDistance(result.distance);
          setDuration(result.duration);
        }}
      />
    );
  const showRouteView = () => (
    <View style={styles.coordsSelectView}>
      <View style={styles.CSV_1}>
        <Image
          source={require('../assets/origin2.png')}
          style={styles.imageOrigin}
        />
        <Image
          source={require('../assets/coordsLine.png')}
          style={styles.imageCordsLine}
        />
        <Image
          source={require('../assets/destination2.png')}
          style={styles.imageDestination}
        />
      </View>
      <View style={styles.CSV_2}>
        <TouchableOpacity
          style={styles.pickupView}
          onPress={() => locationSelectHandler('pickup')}>
          <Text style={styles.textLocation}>Pickup Location</Text>
          <TextTicker
            style={styles.textPickup}
            duration={8000}
            loop
            bounce
            repeatSpacer={50}
            marqueeDelay={3000}>
            {location.pickup}
          </TextTicker>
        </TouchableOpacity>
        <View style={styles.horizontalLine}></View>
        <TouchableOpacity
          style={styles.dropView}
          onPress={() => locationSelectHandler('drop')}>
          <Text style={styles.textLocation}>Drop Location</Text>
          <TextTicker
            style={styles.textDrop}
            duration={8000}
            loop
            bounce
            repeatSpacer={50}
            marqueeDelay={3000}>
            {location.drop}
          </TextTicker>
        </TouchableOpacity>
      </View>
    </View>
  );
  const showAmbulanceFlatList = () =>
    ![
      originCoords.latitude,
      originCoords.longitude,
      destinationCoords.latitude,
      destinationCoords.latitude,
    ].includes(0) && (
      <View style={styles.flatView}>
        <FlatList
          style={styles.flatList}
          data={ambulanceState.list}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
    );
  const showBookingButton = () =>
    ambulanceState.selected.id !== '' &&
    ![
      originCoords.latitude,
      originCoords.longitude,
      destinationCoords.latitude,
      destinationCoords.latitude,
    ].includes(0) && (
      <View style={styles.btn_view}>
        <TouchableOpacity
          style={styles.btn_touch}
          activeOpacity={0.5}
          onPress={() => modalHandler('PaymentOption')}>
          <Text style={styles.BookButtonText}>BOOK NOW</Text>
        </TouchableOpacity>
        <View style={bookingFor.show && styles.btn_view_2}>
          {bookingFor.show && (
            <TouchableOpacity
              style={styles.btn_touch}
              onPress={() => modalHandler('SomeoneBooking')}>
              <Text style={styles.BookButtonText}>FOR SOMEONE</Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity
            style={[styles.btn_touch]}
            onPress={() => modalHandler('BookingLater')}>
            <Text style={styles.BookButtonText}>SCHEDULE</Text>
          </TouchableOpacity>
        </View>
      </View>
    );

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.item}
      key={`child_key${item.id}`}
      onPress={() => {
        const wallet_amount = +item.base_fare;
        setAmbulanceState({...ambulanceState, selected: item, wallet_amount});
      }}>
      <Image source={{uri: item.image}} style={styles.image_1} />
      <Text style={styles.textTitle}>{item.name}</Text>
      <Text style={styles.textPrice}>{textInPrice(item.base_fare)}</Text>
      {item.price_id === ambulanceState.selected.price_id && (
        <Image source={require('../assets/tick.png')} style={styles.amb_tick} />
      )}
    </TouchableOpacity>
  );
  const showModal = () => (
    <Modal
      animationType="slide"
      transparent={true}
      visible={stateModal.show}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={modalStyle.container}>
        <View style={modalStyle.modalView}>
          <Image
            source={require('../assets/covid.png')}
            style={modalStyle.image}
          />
          <Text style={modalStyle.title}>Select Ambulance Type</Text>
          <RadioForm formHorizontal={false} animation={true}>
            {stateModal.props.map((obj, i) => (
              <RadioButton labelHorizontal={true} key={i}>
                <RadioButtonInput
                  obj={obj}
                  index={i}
                  isSelected={stateModal.type === i}
                  onPress={(value) =>
                    setStateModal({...stateModal, type: value})
                  }
                  borderWidth={1}
                  buttonInnerColor={'#EF4236'}
                  buttonOuterColor={
                    stateModal.type === i ? '#EF4236' : '#0A1F44'
                  }
                  buttonSize={10}
                  buttonOuterSize={20}
                  buttonStyle={{}}
                  buttonWrapStyle={radioStyle.buttonWrapStyle}
                />
                <RadioButtonLabel
                  obj={obj}
                  index={i}
                  labelHorizontal={true}
                  onPress={(value) =>
                    setStateModal({...stateModal, type: value})
                  }
                  labelStyle={radioStyle.labelStyle}
                  labelWrapStyle={radioStyle.labelWrapStyle}
                />
              </RadioButton>
            ))}
          </RadioForm>
          <TouchableOpacity style={modalStyle.touch} onPress={navigateHandler}>
            <Text style={modalStyle.touchText}>NEXT</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
  const modalHandler = (navigate) => {
    if (navigate === 'SomeoneBooking') navigation.navigate(navigate);
    else setStateModal({...stateModal, navigate, show: true});
  };
  const navigateHandler = async () => {
    const {navigate, type} = stateModal;
    const {selected, wallet_amount} = ambulanceState;
    setStateModal({...stateModal, show: false, navigate: ''});
    if (navigate) {
      console.log('navigate', navigate);
      const body = {
        price_id: selected.price_id,
        is_covid_booking: type === 0 ? '1' : '0',
        total_km: Math.round(distance),
        waiting_minutes: Math.round(duration),
      };
      // console.log(body);
      const result = await EstimateApi(body);
      if (result.status) {
        navigation.navigate(navigate, {
          ambulance: selected,
          wallet_amount,
          navigate,
          covid: type === 0,
          originCoords,
          destinationCoords,
          distance,
          bookingFor,
          schedule: {
            date: '',
            time: '',
          },
          result,
        });
      } else {
        alert('Something went wrong');
      }
    } else {
      console.error('navblank');
    }
  };
  useEffect(() => {
    setOriginCoords({
      latitude: region.latitude,
      longitude: region.longitude,
    });
    updateLocation();
  }, []);

  useEffect(() => {
    if (
      route.params.locationType &&
      Object.keys(route.params).includes('location')
    ) {
      const coords = {
        latitude: route.params.location.lat,
        longitude: route.params.location.lng,
      };
      route.params.locationType === 'pickup'
        ? setOriginCoords(coords)
        : setDestinationCoords(coords);
      setLocation({
        ...location,
        [route.params.locationType]: route.params.description,
      });
    }
    if (route.params.mobile) {
      setBookingFor({
        ...bookingFor,
        show: false,
        type: 'member',
        mobile: route.params.mobile.replace(' ', ''),
      });
    }
  }, [route.params]);

  useEffect(() => {
    dispatch(actions.SetLocation(location));
  }, [location]);

  useEffect(() => {
    if (![originCoords.latitude, originCoords.longitude].includes(0)) {
      const body = {
        user_id: user.id,
        lat: originCoords.latitude,
        long: originCoords.longitude,
        type: 'book_now',
      };
      AmbulanceCategoryApi(body).then((res) => {
        if (res.status) {
          setAmbulanceState({...ambulanceState, list: res.list});
        }
      });
    }
  }, [originCoords.latitude, originCoords.longitude]);
  const updateLocation = async () => {
    if (region.latitude !== 0 && region.longitude !== 0) {
      const res = await GeocoderLocation(region.latitude, region.longitude);
      setLocation({...location, pickup: res[0].formattedAddress});
    }
  };
  const locationSelectHandler = (locationType) =>
    navigation.navigate('AmbulanceBookingLocation', {locationType, region});

  return (
    <SafeAreaView style={viewStyle.container}>
      {showBackButton()}
      {showTitle()}
      <View style={styles.mapView}>
        {showMapView()}
        {showRouteView()}
        {showAmbulanceFlatList()}
        {showBookingButton()}
      </View>
      {showModal()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mapView: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
  title_view_55: {
    flexDirection: 'row',
    marginHorizontal: 30,
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 5,
  },
  coordsSelectView: {
    position: 'absolute',
    top: '1%',
    width: '85%',
    alignSelf: 'center',
    borderRadius: 16,
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'row',
  },
  CSV_1: {
    flex: 0.25,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingVertical: '5%',
  },
  imageOrigin: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },
  imageCordsLine: {
    width: 2,
    resizeMode: 'cover',
  },
  imageDestination: {
    width: 16,
    height: 22,
    resizeMode: 'contain',
  },
  CSV_2: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontalLine: {
    width: '100%',
    height: 1,
    backgroundColor: '#EFEFEF',
  },
  pickupView: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 16,
  },
  textPickup: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 17,
    color: '#242E42',
  },
  dropView: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 16,
  },
  textDrop: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 17,
    color: '#242E42',
  },

  textLocation: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 13,
    color: '#ABAFBD',
  },
  flatView: {
    position: 'absolute',
    bottom: 110,
    width: '100%',
  },
  btn_view: {
    width: '80%',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 5,
  },
  btn_view_2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  btn_touch: {
    backgroundColor: '#EF4236',
    borderRadius: 8,
    paddingHorizontal: 10,
    paddingVertical: 8,
    marginBottom: 10,
    minWidth: 140,
  },
  BookButtonText: {
    fontFamily: 'Avenir',
    color: '#FFFFFF',
    fontWeight: '900',
    fontSize: 15,
    textAlign: 'center',
  },
  image_1: {
    width: 120,
    height: 60,
    alignSelf: 'center',
  },
  textTitle: {
    margin: 10,
    fontFamily: 'Avenir',
    color: '#000000',
    fontWeight: '500',
    fontSize: 12,
  },
  textPrice: {
    marginHorizontal: 10,
    marginBottom: 10,
    fontFamily: 'Ping Fang SC',
    color: '#EF4236',
    fontWeight: '600',
    fontSize: 12,
  },
  item: {
    marginHorizontal: 12,
    borderRadius: 25,
    backgroundColor: '#FFF',
    width: 120,
    overflow: 'hidden',
  },
  amb_tick: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    position: 'absolute',
    top: 10,
    right: 10,
  },
  markerIcon: {width: 40, height: 40, resizeMode: 'contain'},
});

const modalStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00000099',
    justifyContent: 'center',
  },
  modalView: {
    borderRadius: 8,
    backgroundColor: 'white',
    marginHorizontal: 30,
    paddingVertical: 20,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#0A1F44',
    alignSelf: 'center',
  },
  subTitle: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 15,
    color: '#000000',
    alignSelf: 'center',
    marginTop: 5,
  },
  subTitle_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 14,
    color: '#00000080',
    alignSelf: 'center',
    marginVertical: 5,
    marginHorizontal: 30,
    textAlign: 'center',
  },
  wallet_view: {
    marginTop: 5,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  wallet_cancel: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#0A1F4480',
  },
  wallet_add: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#EF4236',
  },
  image: {
    height: 70,
    width: 70,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginBottom: 15,
  },
  touch: {
    backgroundColor: '#EF4236',
    borderRadius: 8,
    width: 100,
    paddingVertical: 10,
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  touchText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#FFFFFF',
  },
});

export default AmbulanceBooking;
