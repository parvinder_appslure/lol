import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Text,
  View,
  ScrollView,
} from 'react-native';
import store from '../redux/store';
import TextTicker from 'react-native-text-ticker';
import {buttonStyle, imageStyle, textStyle, viewStyle} from '../style/style';
import {textInPrice} from '../backend/Api';
const BookingSummary = ({navigation}) => {
  const showBackButton = () => (
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <Image
        style={imageStyle.navigationGoBack}
        source={require('../assets/Close.png')}
      />
    </TouchableOpacity>
  );
  const showTitle = () => <Text style={textStyle.title}>Summary</Text>;
  const showRouteView = () => (
    <View style={styles.coordsSelectView}>
      <View style={styles.CSV_1}>
        <Image
          source={require('../assets/origin2.png')}
          style={styles.imageOrigin}
        />
        <Image
          source={require('../assets/coordsLine.png')}
          style={styles.imageCordsLine}
        />
        <Image
          source={require('../assets/destination2.png')}
          style={styles.imageDestination}
        />
      </View>
      <View style={styles.CSV_2}>
        <TouchableOpacity
          style={styles.pickupView}
          onPress={() => locationSelectHandler('pickup')}>
          <Text style={styles.textLocation}>Pickup Location</Text>
          <TextTicker
            style={styles.textPickup}
            duration={8000}
            loop
            bounce
            repeatSpacer={50}
            marqueeDelay={3000}>
            {store.getState().location.pickup}
          </TextTicker>
        </TouchableOpacity>
        <View style={styles.horizontalLine}></View>
        <TouchableOpacity
          style={styles.dropView}
          //   onPress={() => locationSelectHandler('drop')}
        >
          <Text style={styles.textLocation}>Drop Location</Text>
          <TextTicker
            style={styles.textDrop}
            duration={8000}
            loop
            bounce
            repeatSpacer={50}
            marqueeDelay={3000}>
            {store.getState().location.drop}
          </TextTicker>
        </TouchableOpacity>
      </View>
    </View>
  );
  const showAmbulanceType = () => (
    <>
      <Text style={styles.subTitleText}>Ambulance Type</Text>
      <View style={styles.ambulanceView}>
        <Image
          source={require('../assets/ambulnce.png')}
          style={styles.ambulanceImage}
        />
        <View style={{margin: 14}}>
          <Text style={styles.ambulanceTypeText}>Normal Ambulance</Text>
          <Text style={styles.amountText}>{textInPrice('3000')}</Text>
        </View>
      </View>
    </>
  );
  const showDateTime = () => (
    <>
      <Text style={styles.subTitleText}>Date & Time</Text>
      <View style={styles.dateTimeView}>
        <View style={styles.dateTimeView_2}>
          <Image
            source={require('../assets/calendar.png')}
            style={styles.dateTimeIcon}
          />
          <Text style={styles.dateText}>3 MAY 2020</Text>
        </View>
        <View style={styles.dateTimeView_2}>
          <Image
            source={require('../assets/clock.png')}
            style={styles.dateTimeIcon}
          />
          <Text style={styles.timeText}>10:00 am</Text>
        </View>
      </View>
    </>
  );
  const showPayment = () => (
    <>
      <Text style={styles.subTitleText}>Payment</Text>
      <View style={styles.paymentView}>
        <View style={styles.paymentView_2}>
          <Text style={styles.paymentText}>Estimate Amount</Text>
          <Text style={styles.paymentAmountText}>{textInPrice('3000')}</Text>
        </View>
        <View style={styles.paymentView_2}>
          <Text style={styles.paymentText}>Booking Amount</Text>
          <Text style={styles.paymentAmountText}>{textInPrice('3000')}</Text>
        </View>
        <View style={styles.paymentView_2}>
          <Text style={styles.gstText}>(10% of est amount)</Text>
        </View>
      </View>
    </>
  );
  const showPayNow = () => (
    <TouchableOpacity
      style={buttonStyle.submit}
      activeOpacity={0.5}
      onPress={() => navigation.navigate('PaymentMethod')}>
      <Text style={textStyle.button}>PAY NOW</Text>
    </TouchableOpacity>
  );
  return (
    <SafeAreaView style={viewStyle.container}>
      <ScrollView>
        {showBackButton()}
        {showTitle()}
        {showRouteView()}
        {showAmbulanceType()}
        {showDateTime()}
        {showPayment()}
        {showPayNow()}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  subTitleText: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 18,
    color: '#0A1F44',
    marginHorizontal: 30,
    marginVertical: 30,
  },
  coordsSelectView: {
    flex: 0,
    width: '85%',
    alignSelf: 'center',
    borderRadius: 16,
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    marginTop: 30,
    elevation: 1,
  },
  horizontalLine: {
    width: '100%',
    height: 1,
    backgroundColor: '#EFEFEF',
  },
  CSV_1: {
    flex: 0.25,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingVertical: '5%',
  },
  imageOrigin: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },
  imageCordsLine: {
    width: 2,
    resizeMode: 'cover',
  },
  imageDestination: {
    width: 16,
    height: 22,
    resizeMode: 'contain',
  },
  CSV_2: {
    flex: 1,
    justifyContent: 'center',
  },
  pickupView: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 16,
  },
  textPickup: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 17,
    color: '#242E42',
  },
  dropView: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 16,
  },
  textDrop: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 17,
    color: '#242E42',
  },
  amountText: {
    marginTop: 8,
    fontFamily: 'Ping Fang SC',
    fontWeight: '600',
    fontSize: 16,
    color: '#EF4236',
  },
  ambulanceTypeText: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 16,
    color: '#000000',
  },
  ambulanceImage: {
    height: 90,
    width: 130,
    opacity: 0.95,
    resizeMode: 'cover',
    borderTopLeftRadius: 16,
    borderBottomLeftRadius: 16,
  },
  ambulanceView: {
    flex: 0,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    marginHorizontal: 30,
    borderRadius: 16,
    elevation: 1,
  },
  dateText: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 15,
    color: '#282727',
  },
  timeText: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 15,
    color: '#282727',
  },
  dateTimeIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginHorizontal: 10,
  },
  dateTimeView: {
    flex: 0,
    backgroundColor: '#FFFFFF',
    marginHorizontal: 30,
    borderRadius: 16,
    elevation: 1,
  },
  dateTimeView_2: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 16,
    marginVertical: 8,
  },
  paymentView: {
    flex: 0,
    backgroundColor: '#FFFFFF',
    marginHorizontal: 30,
    borderRadius: 16,
    paddingVertical: 8,
    elevation: 1,
  },
  paymentView_2: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 16,
  },
  paymentText: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 15,
    color: '#282727',
  },
  paymentAmountText: {
    marginTop: 8,
    fontFamily: 'Ping Fang SC',
    fontWeight: '600',
    fontSize: 16,
    color: '#EF4236',
  },
  gstText: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 11,
    color: '#EF4236',
  },
});
export default BookingSummary;
