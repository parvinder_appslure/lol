import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  View,
} from 'react-native';
import {
  Height,
  formatAmount,
  formatNumber,
  textInPrice,
  AddWalletApi,
  GetProfileApi,
} from '../backend/Api';
import {imageStyle, viewStyle, textStyle, buttonStyle} from '../style/style';

import Paytm from '@philly25/react-native-paytm';
import {checkSumApi} from '../backend/GatewayApi';
import {useDispatch, useSelector} from 'react-redux';
import * as actions from '../redux/actions';
const Wallet = ({navigation, route}) => {
  const {user, deviceInfo} = useSelector((state) => state);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    show: 'wallet',
    amount: route.params ? route.params.toString() : '',
  });

  const onPressHandler = (amount) =>
    setState({...state, amount: formatAmount(amount)});

  const touchableButton = (text) => (
    <TouchableOpacity
      style={styles[formatNumber(state.amount) === text ? 'view_3' : 'view_2']}
      onPress={() => onPressHandler(text)}>
      <Text
        style={[
          styles.text_3,
          formatNumber(state.amount) === text && styles.text_4,
        ]}>
        {textInPrice(text)}
      </Text>
    </TouchableOpacity>
  );

  const addMoneyHandler = () => {
    const {id, mobile, email} = user;
    const config = {
      orderId: `OID_${id}_${new Date().getTime()}`,
      customerId: id,
      mobile,
      email,
      taxAmt: +formatNumber(state.amount),
    };
    if (config.taxAmt > 0) {
      checkSumApi(config)
        .then((details) => Paytm.startPayment(details))
        .catch((error) => console.log(error));
    }
  };
  const walletUpdate = async (user_id) => {
    const {status, user} = await GetProfileApi({
      user_id,
      device_id: deviceInfo.id,
      device_type: deviceInfo.os,
      device_token: deviceInfo.token,
    });
    if (status) {
      dispatch(actions.Login(user));
      setTimeout(() => {
        if (route.params) {
          navigation.goBack();
        } else {
          navigation.reset({
            index: 0,
            routes: [{name: 'MyDrawer'}],
          });
        }
      }, 2000);
    }
  };
  const onPayTmResponse = async (resp) => {
    setState({...state, show: ''});
    const {STATUS, TXNID, ORDERID, TXNAMOUNT, RESPMSG} = resp;
    console.log(JSON.stringify(resp, null, 2));
    if (STATUS && STATUS === 'TXN_SUCCESS') {
      // Payment succeed!
      const body = {
        user_id: user.id,
        order_id: ORDERID,
        transaction_id: TXNID,
        payment_from: 'paytm',
        amount: TXNAMOUNT,
        status_code: '1',
        description: 'no description',
      };
      const {status = false} = await AddWalletApi(body);
      if (status === 'success') {
        setState({...state, show: 'success'});
        walletUpdate(user.id);
      } else {
        alert('Something went wrong please try again');
        setState({...state, show: 'wallet'});
      }
    } else {
      alert(
        RESPMSG === 'System Error'
          ? 'System Error. Please try again later after some time'
          : 'Your Transaction has been failed please try again',
      );
      setState({...state, show: 'wallet'});
    }
  };

  useEffect(() => {
    Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    return () => {
      Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    };
  }, []);
  return (
    <SafeAreaView style={[viewStyle.container, {minHeight: Height}]}>
      {state.show === 'wallet' && (
        <>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={imageStyle.navigationGoBack}
              source={require('../assets/Close.png')}
            />
          </TouchableOpacity>
          <Text style={textStyle.title}>Wallet</Text>
          <Image
            style={styles.image_1}
            source={require('../assets/Group.png')}
          />
          <Text style={styles.text_1}>{`Available Balance: ${textInPrice(
            user.wallet_amount,
          )}`}</Text>
          <Text style={styles.text_2}>Enter Amount</Text>
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            value={state.amount}
            onChangeText={(text) => {
              let amount = formatNumber(text);
              setState({
                ...state,
                amount: `${+amount ? formatAmount(amount) : ''}`,
              });
            }}
          />
          <View style={styles.view_1}>
            {touchableButton('500')}
            {touchableButton('1000')}
            {touchableButton('1500')}
          </View>
          <TouchableOpacity
            style={buttonStyle.submitMarginAuto}
            activeOpacity={0.5}
            onPress={addMoneyHandler}>
            <Text style={textStyle.button}>ADD MONEY</Text>
          </TouchableOpacity>
        </>
      )}
      {state.show === 'success' && (
        <View style={styles.success_container}>
          <Image
            source={require('../assets/tick.png')}
            style={styles.success_image}
          />
          <Text style={styles.success_text_1}>Thank You !</Text>
          <Text style={styles.success_text_2}>
            Money Added Successful to your wallet.
          </Text>
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  image_1: {
    height: 92,
    width: 92,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  text_1: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 20,
    color: '#0A1F44',
    alignSelf: 'center',
    marginTop: 15,
  },
  text_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 16,
    color: '#ACB1C0',
    marginTop: 30,
    marginHorizontal: 25,
  },
  text_3: {
    fontFamily: 'Ping Fang SC',
    fontWeight: '600',
    fontSize: 18,
    color: '#1E2432',
    marginVertical: 10,
    marginHorizontal: 16,
  },
  text_4: {
    color: '#FFFFFF',
  },
  textInput: {
    marginHorizontal: 25,
    borderBottomWidth: 1,
    borderColor: '#EAECEF',
    fontFamily: 'Ping Fang SC',
    fontWeight: '600',
    fontSize: 22,
    color: '#1E2432',
  },
  view_1: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 25,
  },
  view_2: {
    borderColor: '#D0D0D0',
    borderWidth: 1,
    borderRadius: 4,
  },
  view_3: {
    backgroundColor: '#EF4236',
    borderRadius: 4,
  },
  success_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  success_image: {
    height: 100,
    width: 100,
    resizeMode: 'cover',
  },
  success_text_1: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 22,
    color: '#0A1F44',
    marginVertical: 16,
  },
  success_text_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    color: '#000000',
  },
});
export default Wallet;
