import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import {buttonStyle, imageStyle, textStyle, viewStyle} from '../style/style';
import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';
import {useDebounce} from 'use-debounce';

const SomeoneBooking = ({navigation, route}) => {
  const [state, setState] = useState({
    contacts: [],
    filter: [],
    select: [],
    search: '',
    recordID: '',
  });
  const [value] = useDebounce(state.search, 400);

  const showBackButton = () => (
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <Image
        style={imageStyle.navigationGoBack}
        source={require('../assets/Close.png')}></Image>
    </TouchableOpacity>
  );
  const showTitle = () => <Text style={textStyle.title}>Choose Contact</Text>;
  const contactPermission = async () => {
    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
      {
        title: 'Contacts Permission',
        message: 'This app would like to view your contacts.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (status === 'granted') {
      Contacts.getAll().then((contacts) => {
        contacts = contacts.filter((item) => item.phoneNumbers.length);
        setState({...state, contacts, filter: contacts});
      });
    }
  };

  const buttonHandler = () => {
    const {recordID, select} = state;
    if (recordID === '') {
      alert('Please Select the Number');
      return;
    }
    const {phoneNumbers = []} = select;
    if (phoneNumbers.length === 0) {
      alert('Invalid Phone Number');
      return;
    }
    const {number = ''} = phoneNumbers[0];
    if (number === '') {
      alert('Please make sure primary number of this contact is not empty');
      return;
    }
    navigation.navigate('AmbulanceBooking', {
      mobile: number,
    });
  };

  useEffect(() => {
    contactPermission();
  }, []);
  useEffect(() => {
    const filter = state.contacts.filter((item) => {
      const search =
        item.phoneNumbers[0].number + item.displayName.toLowerCase();
      return search.includes(value.toLowerCase());
    });
    setState({...state, filter});
  }, [value]);
  return (
    <SafeAreaView style={viewStyle.container}>
      {showBackButton()}
      {showTitle()}
      <View style={styles.ph_view}>
        <TextInput
          style={styles.ph_textInput}
          keyboardType="default"
          onChangeText={(text) => setState({...state, search: text})}
          value={state.search}
          maxLength={10}></TextInput>
        <Image
          source={require('../assets/phoneBook.png')}
          style={styles.ph_image}
        />
      </View>
      <FlatList
        data={state.filter}
        keyExtractor={(item) => item.recordID.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            key={`t_${item.recordID}`}
            style={[
              styles.cn_touch,
              item.recordID === state.recordID && styles.cn_selected,
            ]}
            onPress={() =>
              setState({
                ...state,
                select: item,
                recordID: item.recordID,
              })
            }>
            <Image
              source={require('../assets/userav.png')}
              style={styles.cn_image}
            />
            <View style={styles.cn_view}>
              <Text style={styles.cn_name}>{item.displayName}</Text>
              <Text style={styles.cn_phone}>{item.phoneNumbers[0].number}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
      <TouchableOpacity
        style={buttonStyle.submitMarginAuto}
        onPress={buttonHandler}>
        <Text style={textStyle.button}>CONTINUE</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default SomeoneBooking;

const styles = StyleSheet.create({
  ph_view: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#EAECEF',
    borderBottomWidth: 1,
    margin: 20,
    paddingHorizontal: 20,
  },
  ph_textInput: {
    flex: 1,
    paddingHorizontal: 20,
    fontFamily: 'Avenir-Medium',
    fontSize: 20,
    fontWeight: '500',
    color: '#1E2432',
  },
  ph_image: {
    height: 22,
    width: 22,
    resizeMode: 'contain',
  },
  cn_touch: {
    flexDirection: 'row',
    paddingHorizontal: 30,
    paddingVertical: 10,
    alignItems: 'center',
  },
  cn_image: {
    height: 50,
    width: 50,
    borderRadius: 25,
    resizeMode: 'contain',
  },
  cn_view: {
    paddingHorizontal: 20,
  },
  cn_name: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 17,
    color: '#000000',
    paddingVertical: 2,
  },
  cn_phone: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    color: '#00000080',
    paddingVertical: 2,
  },
  cn_selected: {
    backgroundColor: 'pink',
  },
});
