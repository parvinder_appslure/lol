import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';
import {imageStyle, textStyle, viewStyle} from '../style/style';

const AboutUs = ({navigation}) => {
  return (
    <SafeAreaView style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/Close.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>About Us</Text>
      <Image source={require('../assets/wel-logo.png')} style={styles.logo} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  logo: {
    marginVertical: 40,
    width: 180,
    height: 102,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
});
export default AboutUs;
