import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  Linking,
} from 'react-native';

import {useDispatch, useSelector} from 'react-redux';
import * as actions from '../redux/actions';
import {
  GeolocationInfo,
  BookingStatusForHomeApi,
  CancelBookingApi,
  CancelBookingUserApi,
} from '../backend/Api';
import Loader from './Loader';
import {viewStyle} from '../style/style';
import TextTicker from 'react-native-text-ticker';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
import {useIsFocused} from '@react-navigation/native';
import TimerCounter from './TimerCounter';
const Main = ({navigation}) => {
  const {user, netInfo} = useSelector((state) => state);
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const [state, setState] = useState({
    ambulance_booking_flag: -1,
    details: {},
    ambulence_booking_cancel_charge: 0,
  });
  const [loading, setLoading] = useState(false);
  const showHeaderView = () => (
    <View style={styles.headerView}>
      <TouchableOpacity onPress={navigation.openDrawer}>
        <Image
          source={require('../assets/menu.png')}
          style={styles.imageMenu}
        />
      </TouchableOpacity>
      <View>
        <Text style={styles.hd_title}>{`Hi ${user.name}`}</Text>
        {/* ${state.ambulance_booking_flag} */}
      </View>
      <TouchableOpacity
        style={styles.headerSearchView}
        onPress={() => navigation.navigate('Search')}>
        <Image
          source={require('../assets/search.png')}
          style={styles.imageSearch}
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.headerNotificationView}
        onPress={() => navigation.navigate('Notification')}>
        <Image
          source={require('../assets/notification.png')}
          style={styles.imageNotification}
        />
      </TouchableOpacity>
    </View>
  );
  const showScrollView = () => (
    <ScrollView>
      {/* {showBookingStatus()} */}
      {showBloodDonation()}
      <View style={styles.view_main}>
        {optionView(
          require('../assets/ambulance.png'),
          'Ambulance Booking',
          () => navigation.navigate('AmbulanceBooking', {}),
        )}
        {optionView(
          require('../assets/blood-donation.png'),
          'Blood Donation',
          () => {},
        )}
        {optionView(require('../assets/donation.png'), 'Donation', () => {})}
        {optionView(require('../assets/about-menu.png'), 'About LOL', () => {})}
      </View>
    </ScrollView>
  );
  const showBloodDonation = () => (
    <View style={styles.bdr_mainView}>
      <Text style={styles.bdr_title}>Blood Donation Request</Text>
      <View style={styles.bdr_sub_view}>
        <View style={styles.bdr_sub_view_1}>
          <Text style={styles.bdr_time}>Date : 22/08/2020</Text>
          <Text style={styles.bdr_time}>Time : 04:00 PM</Text>
        </View>
        <View style={styles.bdr_sub_view_2}>
          <Image
            style={styles.bdr_image}
            source={require('../assets/user.png')}
          />
          <View style={styles.bdr_user_view}>
            <Text style={styles.bdr_title}>Karan Patel</Text>
            <Text style={styles.bdr_group}>Group: B+</Text>
            <Text style={styles.bdr_group}>Units: 4</Text>
          </View>
          <View style={styles.bdr_touch}>
            <TouchableOpacity style={styles.bdr_touch_accept}>
              <Text style={styles.bdr_accept}>Accept</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.bdr_touch_reject}>
              <Text style={styles.bdr_accept}>Reject</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
  const showBookingStatus = () => {
    const flag = state.ambulance_booking_flag;
    const {source_address = '', destination_address = ''} = state.details;
    if (flag === 0)
      return (
        <View style={styles.na_container}>
          <Image
            style={styles.na_image}
            source={require('../assets/stop.png')}
          />
          <Text style={styles.na_title}>{`You have no active\nbooking`}</Text>
        </View>
      );
    if ([1, 2].includes(flag))
      return (
        <View style={styles.na_container}>
          <Text style={styles.na_title_active}>Active Booking</Text>
          <View style={styles.coordsSelectView}>
            <View style={styles.CSV_1}>
              <Image
                source={require('../assets/origin2.png')}
                style={styles.imageOrigin}
              />
              <Image
                source={require('../assets/coordsLine.png')}
                style={styles.imageCordsLine}
              />
              <Image
                source={require('../assets/destination2.png')}
                style={styles.imageDestination}
              />
            </View>
            <View style={styles.CSV_2}>
              <View style={styles.textTickerView}>
                <Text style={styles.textLocation}>Pickup Location</Text>
                <TextTicker
                  style={styles.textTicker}
                  duration={8000}
                  loop
                  bounce
                  repeatSpacer={50}
                  marqueeDelay={3000}>
                  {source_address}
                </TextTicker>
              </View>
              <View style={styles.horizontalLine}></View>
              <View style={styles.textTickerView}>
                <Text style={styles.textLocation}>Drop Location</Text>
                <TextTicker
                  style={styles.textTicker}
                  duration={8000}
                  loop
                  bounce
                  repeatSpacer={50}
                  marqueeDelay={3000}>
                  {destination_address}
                </TextTicker>
              </View>
            </View>
          </View>
        </View>
      );
  };
  const optionView = (source, h1, onPress) => (
    <TouchableOpacity style={styles.opt_view} onPress={onPress}>
      <Image source={source} style={styles.opt_image} />
      <View style={styles.opt_view_2}>
        <Text style={styles.opt_h1}>{h1}</Text>
      </View>
    </TouchableOpacity>
  );
  const statusDecode = (status) => {
    const obj = {
      status_0: 'Ambulance is getting assigned',
      status_9: 'Ambulance is on the way',
      status_6: 'Your Ambulance has Arrived',
      status_7: 'You are riding towards the Destination',
      status_8: 'Your Destination has Arrived! Best of luck',
    };
    return obj[`status_${status}`];
  };
  const showDriverView = () => {
    const {status = 0} = state.details;
    const flag = state.ambulance_booking_flag;
    const {
      source_address = '',
      destination_address = '',
      driver_name,
      vehicle_number,
      driver_image,
      driver_mobile,
    } = state.details;
    // console.log(JSON.stringify(state.details, null, 2));
    // console.log('status', status);
    if (flag === -1) return <></>;
    else
      return (
        <View style={styles.dv_container}>
          <View style={styles.na_container}>
            <Text style={styles.na_title_active}>Active Booking</Text>
            <View style={styles.coordsSelectView}>
              <View style={styles.CSV_1}>
                <Image
                  source={require('../assets/origin2.png')}
                  style={styles.imageOrigin}
                />
                <Image
                  source={require('../assets/coordsLine.png')}
                  style={styles.imageCordsLine}
                />
                <Image
                  source={require('../assets/destination2.png')}
                  style={styles.imageDestination}
                />
              </View>
              <View style={styles.CSV_2}>
                <View style={styles.textTickerView}>
                  <Text style={styles.textLocation}>Pickup Location</Text>
                  <TextTicker
                    style={styles.textTicker}
                    duration={8000}
                    loop
                    bounce
                    repeatSpacer={50}
                    marqueeDelay={3000}>
                    {source_address}
                  </TextTicker>
                </View>
                <View style={styles.horizontalLine}></View>
                <View style={styles.textTickerView}>
                  <Text style={styles.textLocation}>{`Drop Location`}</Text>
                  <TextTicker
                    style={styles.textTicker}
                    duration={8000}
                    loop
                    bounce
                    repeatSpacer={50}
                    marqueeDelay={3000}>
                    {destination_address}
                  </TextTicker>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 20,
              paddingVertical: 10,
            }}>
            <View
              style={flag === 2 ? styles.dv_leftView : styles.dv_leftView_2}>
              {flag === 1 && (
                <Text style={styles.dv_requestSent}>{`Request Sent`}</Text>
              )}
              {flag === 1 && <TimerCounter />}
              {flag === 1 && (
                <TouchableOpacity
                  style={styles.dv_cancelTouch}
                  onPress={cancelRequestHandler}>
                  <Text style={styles.dv_cancelText}>Cancel</Text>
                </TouchableOpacity>
              )}
              {flag === 2 && (
                <>
                  <Text style={styles.dv_status}>{statusDecode(status)}</Text>
                  {['6', '7', '8'].includes(status) && (
                    <TouchableOpacity
                      style={styles.dv_loctionTouch}
                      onPress={() => {
                        navigation.navigate('AmbulanceDetails', state.details);
                      }}>
                      <Text style={styles.dv_loction}>Driver Location</Text>
                    </TouchableOpacity>
                  )}
                </>
              )}
            </View>
            {flag === 2 && (
              <View style={styles.dv_rightView}>
                {['0', '9', '6'].includes(status) && (
                  <TouchableOpacity
                    style={styles.dv_cancelTouch}
                    onPress={cancelRequestHandler}>
                    <Text style={styles.dv_cancelText}>Cancel</Text>
                  </TouchableOpacity>
                )}
              </View>
            )}
          </View>
          {['9', '6', '7', '8'].includes(status) && (
            <View style={styles.st_container}>
              <Image
                source={{uri: driver_image}}
                // source={require('../assets/user.png')}
                style={styles.st_driverImage}
              />

              <Text
                style={
                  styles.st_text_1
                }>{`${driver_name}\n${vehicle_number}`}</Text>
              <TouchableOpacity
                style={styles.st_callTouch}
                onPress={() => {
                  const url = 'tel:' + driver_mobile;
                  Linking.openURL(url);
                }}>
                <Image
                  style={styles.st_callImage}
                  source={require('../assets/Call.png')}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      );
  };

  const updateLocation = async () => {
    const info = await GeolocationInfo();
    dispatch(actions.SetCoords(info.coords));
  };

  useEffect(() => {
    updateLocation();
  }, []);

  useEffect(() => {
    let timer;
    const callback = async () => {
      let ms = 30000;
      if (netInfo.isInternetReachable) {
        const {
          status = false,
          ambulance_booking_flag,
          details = {},
          ambulence_booking_cancel_charge = 0,
        } = await BookingStatusForHomeApi({user_id: user.id});
        if (status) {
          ms = 5000;
          // console.log('ambulance_booking_flag', ambulance_booking_flag);
          setState({
            ...state,
            ambulance_booking_flag,
            details,
            ambulence_booking_cancel_charge,
          });
        }
      }
      timer = setTimeout(callback, ms);
    };
    if (isFocused) {
      console.log('focused');
      callback();
    } else {
      console.log('unfocused');
      clearTimeout(timer);
    }
    return () => {
      console.log('unmount main component');
      clearTimeout(timer);
    };
  }, [isFocused]);

  useEffect(() => {
    console.log('ambulance_booking_flag', state.ambulance_booking_flag);
    // console.log(JSON.stringify(state.details, null, 2));
  }, [state.ambulance_booking_flag]);

  const cancelRequestHandler = () => {
    const {id} = state.details;
    if (state.ambulance_booking_flag === 1) {
      const cancelRequest = async (body) => {
        setLoading(true);
        const {status = false} = await CancelBookingUserApi(body);
        let title = '',
          subTitle = '';
        if (status) {
          title = 'Cancel Request';
          subTitle = 'Your Booking Request has been cancelled successfully';
          setState({...state, ambulance_booking_flag: 0});
        } else {
          title = 'Cancel Request Failed';
          subTitle =
            'Your Booking Request has been not beed cancelled. Please try again';
        }
        setLoading(false);
        Alert.alert(title, subTitle);
      };
      Alert.alert(
        'Booking Cancel Request',
        `Do you want to cancel this booking request.`,
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Yes', onPress: () => cancelRequest({booking_id: id})},
        ],
        {cancelable: false},
      );
    } else {
      const body = {booking_id: id, user_id: user.id};
      console.log(body);

      const cancelRequest = async () => {
        setLoading(true);
        const {status = false, get_details = {}} = await CancelBookingApi(body);
        let title = '',
          subTitle = '';
        if (status) {
          title = 'Cancel Request';
          subTitle = 'Your Booking Request has been cancelled successfully';
          setState({...state, ambulance_booking_flag: 0});
        } else {
          title = 'Cancel Request Failed';
          subTitle =
            'Your Booking Request has been not beed cancelled. Please try again';
        }
        setLoading(false);
        Alert.alert(title, subTitle);
      };

      Alert.alert(
        'Booking Cancel Request',
        `Do you want to cancel this booking request.\nPlease Note that we will charge \u20B9 ${state.ambulence_booking_cancel_charge} amount as a cancellation charge.`,
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Yes', onPress: cancelRequest},
        ],
        {cancelable: false},
      );
    }
  };

  return (
    <SafeAreaView style={viewStyle.container}>
      <StatusBarDark />
      {loading && <Loader />}
      {showHeaderView()}
      {showScrollView()}
      {[1, 2].includes(state.ambulance_booking_flag) && showDriverView()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  hd_title: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: '900',
    color: '#0A1F44',
  },

  headerView: {
    ...viewStyle.statusBarPadding,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 2,
  },
  headerSearchView: {
    marginLeft: 'auto',
  },
  headerNotificationView: {
    marginHorizontal: 16,
  },
  imageMenu: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
    marginHorizontal: 16,
    marginVertical: 8,
  },
  imageSearch: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
    marginVertical: 8,
  },
  imageNotification: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
    marginVertical: 8,
  },
  view_main: {
    margin: 20,
    marginTop: 0,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  opt_view: {
    width: width / 2 - 30,
    marginVertical: 5,
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 15,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  opt_image: {
    width: 50,
    height: 50,
    marginBottom: 10,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  opt_view_2: {
    marginHorizontal: 5,
  },
  opt_h1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#0A1F44',
    textAlign: 'center',
  },
  bdr_mainView: {
    margin: 20,
  },
  bdr_title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#0A1F44',
  },
  bdr_sub_view: {
    marginTop: 10,
    paddingVertical: 10,
    backgroundColor: 'white',
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  bdr_sub_view_1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: '#9797971a',
    borderBottomWidth: 1,
    paddingVertical: 5,
  },
  bdr_time: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#0A1F44',
    paddingHorizontal: 15,
  },
  bdr_sub_view_2: {
    padding: 10,
    paddingHorizontal: 15,
    flexDirection: 'row',
  },
  bdr_group: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '500',
    fontSize: 14,
    color: '#0A1F4480',
  },
  bdr_accept: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#FFFFFF',
  },
  bdr_touch_accept: {
    backgroundColor: '#00B05F',
    borderRadius: 5,
    paddingVertical: 4,
    paddingHorizontal: 10,
  },
  bdr_touch_reject: {
    backgroundColor: '#F50000',
    borderRadius: 5,
    paddingVertical: 4,
    paddingHorizontal: 10,
  },
  bdr_touch: {
    marginLeft: 'auto',
    justifyContent: 'space-evenly',
  },
  bdr_user_view: {
    justifyContent: 'center',
    marginLeft: 20,
  },
  bdr_image: {
    height: 60,
    width: 60,
    borderRadius: 30,
    resizeMode: 'contain',
    marginVertical: 5,
  },
  dv_container: {
    position: 'absolute',
    // padding: 20,
    bottom: 20,
    width: '90%',
    // height: '15%',
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    // flexDirection: 'row',
    // backgroundColor: 'yellow',
  },
  dv_leftView: {
    justifyContent: 'center',
  },
  dv_leftView_2: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  dv_rightView: {
    marginLeft: 'auto',
    justifyContent: 'center',
  },
  dv_loctionTouch: {
    paddingTop: 2,
  },
  dv_loction: {
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 14,
    color: '#ED4439',
    textDecorationLine: 'underline',
  },
  dv_requestSent: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 15,
    color: '#ED4439',
    textAlignVertical: 'center',
  },
  dv_status: {
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 14,
    color: '#0A1F44cc',
  },
  dv_cancelTouch: {
    backgroundColor: '#F50000',
    padding: 5,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  dv_cancelText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#FFFFFF',
  },
  na_container: {
    // margin: 20,
    // marginVertical: 15,
    // padding: 10,
    // paddingVertical: 20,
    // borderWidth: 2,
    // borderRadius: 25,
    // borderColor: '#F0423680',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  st_container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 30,
    marginVertical: 5,
  },
  st_driverImage: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  st_callTouch: {
    marginLeft: 'auto',
  },
  st_text_1: {
    marginLeft: 10,
    marginVertical: 5,
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 12,
    color: '#0A1F44cc',
  },
  st_callImage: {
    width: 35,
    height: 35,
  },
  na_title_active: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    marginVertical: 5,
    color: '#ED4439',
  },
  na_image: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
    marginBottom: 10,
  },
  na_title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#282727',
    textAlign: 'center',
  },
  coordsSelectView: {
    alignSelf: 'center',
    borderRadius: 16,
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'row',
  },
  CSV_1: {
    flex: 0.2,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  imageOrigin: {
    width: 8,
    height: 8,
    resizeMode: 'contain',
  },
  imageCordsLine: {
    width: 0.5,
    height: 40,
    resizeMode: 'cover',
    marginVertical: 2,
  },
  imageDestination: {
    width: 8,
    height: 8,
    resizeMode: 'contain',
  },
  CSV_2: {
    flex: 1,
    justifyContent: 'center',
  },
  textTickerView: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 16,
  },
  textLocation: {
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 10,
    color: '#ABAFBD',
    marginBottom: 2,
  },
  textTicker: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    color: '#242E42',
  },
  horizontalLine: {
    width: '100%',
    height: 1,
    backgroundColor: '#EFEFEF',
  },
});

export default Main;
