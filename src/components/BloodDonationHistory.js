import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';
import {imageStyle, textStyle, viewStyle} from '../style/style';

const BloodDonationHistory = ({navigation}) => {
  return (
    <SafeAreaView style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/Close.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>Blood Booking History</Text>
      <View style={styles.view_1}>
        <View style={styles.view_2}>
          <Image
            style={styles.image_1}
            source={require('../assets/user.png')}
          />
          <View style={styles.view_4}>
            <Text style={styles.text_1}>Pritampura Blood Bank</Text>
            <Text style={styles.text_2}>Booking Id #12345</Text>
          </View>
          <Image
            style={styles.image_2}
            source={require('../assets/Call.png')}
          />
        </View>
        <View style={styles.view_hz} />
        <Text style={styles.text_3}>Address</Text>
        <Text style={styles.text_4}>
          Building Number B2/456 , Near Deepali chowk, Pritampura, Delhi -
          110034 <Text style={styles.text_5}>(on map)</Text>
        </Text>
        <View style={styles.view_hz} />
        <View style={styles.view_3}>
          <Text style={styles.text_3}>DATE & TIME</Text>
          <Text style={styles.text_3}>OTP</Text>
        </View>
        <View style={styles.view_3}>
          <Text style={styles.text_4}>Tue, 3 June, 2019 08:30 AM</Text>
          <Text style={styles.text_5}>4545</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  view_hz: {
    height: 1,
    backgroundColor: '#9797971a',
    marginVertical: 11,
  },
  view_1: {
    flex: 0,
    margin: 16,
    padding: 16,
    borderRadius: 6,
    borderColor: '#97979766',
    borderWidth: 1,
  },
  view_2: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  view_3: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  view_4: {
    marginLeft: 11,
  },

  image_1: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
  },
  image_2: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    marginLeft: 'auto',
  },
  text_1: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 17,
    lineHeight: 18,
    color: '#1E2432',
    paddingBottom: 4,
  },
  text_2: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 13,
    color: '#1e243280',
  },
  text_3: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 11,
    lineHeight: 18,
    color: '#ACB1C0',
  },
  text_4: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 15,
    lineHeight: 22,
    color: '#1E2432',
  },
  text_5: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 15,
    lineHeight: 22,
    color: '#EF4236',
  },
});
export default BloodDonationHistory;
