import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  FlatList,
  View,
} from 'react-native';
import moment from 'moment';
import {useState} from 'react';
import {buttonStyle, imageStyle, textStyle, viewStyle} from '../style/style';
import DateTimePicker from '@react-native-community/datetimepicker';

const BookingLater = ({navigation, route}) => {
  const [itemState, setItemState] = useState(0);
  const [timeState, setTimeState] = useState({
    time: new Date(),
    show: false,
  });

  const showBackButton = () => (
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <Image
        style={imageStyle.navigationGoBack}
        source={require('../assets/Close.png')}
      />
    </TouchableOpacity>
  );
  const showTitle = () => (
    <Text style={textStyle.title}>Select Date & Time</Text>
  );
  const buttonHandler = () => {
    const {params} = route;
    params.schedule.date = moment().add(itemState, 'days').format('YYYY-MM-DD');
    params.schedule.time = moment(timeState.time).format('HH:mm');
    const {date, time} = params.schedule;
    if (moment(`${date}T${time}`).diff(moment()) / 1000 > 10800) {
      navigation.navigate('PaymentOption', {
        ...params,
      });
    } else {
      alert('You can  only schedule your booking 3 hours before');
    }
  };
  const showContinueButton = () => (
    <TouchableOpacity
      style={buttonStyle.submitMarginAuto}
      activeOpacity={0.5}
      onPress={buttonHandler}>
      <Text style={textStyle.button}>Continue</Text>
    </TouchableOpacity>
  );
  const showDaySlot = () => (
    <>
      <View style={styles.dateView}>
        <Text style={styles.dateText}>Select Date</Text>
        <Text style={styles.dateText}>
          {moment().add(itemState, 'days').format('ddd, DD MMM')}
        </Text>
      </View>
      <View style={styles.flatListView}>
        <FlatList
          contentContainerStyle={styles.flatList}
          horizontal={true}
          data={[0, 1, 2, 3, 4, 5, 6]}
          keyExtractor={(item) => item.toString()}
          renderItem={({item}) => (
            <TouchableOpacity
              key={`dayKey_${item}`}
              style={styles.renderItemView}
              onPress={() => setItemState(+item)}>
              <Text style={styles.dayText}>
                {moment()
                  .add(+item, 'days')
                  .format('ddd')
                  .toUpperCase()}
              </Text>
              <View
                style={
                  styles[itemState === +item ? 'selectView' : 'unSelectView']
                }>
                <Text
                  style={
                    styles[itemState === +item ? 'selectText' : 'unSelectText']
                  }>
                  {moment()
                    .add(+item, 'days')
                    .format('DD')}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </>
  );

  return (
    <SafeAreaView style={viewStyle.container}>
      {showBackButton()}
      {showTitle()}
      {showDaySlot()}
      <TouchableOpacity
        onPress={() => setTimeState({...timeState, show: true})}>
        <View style={styles.dateView}>
          <Text style={styles.dateText}>Select Time</Text>
          <Text style={styles.dateText}>
            {moment(timeState.time).format('hh:mm a')}
          </Text>
        </View>
      </TouchableOpacity>
      {timeState.show && (
        <DateTimePicker
          testID="dateTimePicker"
          minimumDate={new Date()}
          value={timeState.time}
          mode={'time'}
          // is24Hour={true}
          display="default"
          onChange={(event) => {
            if (event.type === 'set') {
              setTimeState({
                time: new Date(event.nativeEvent.timestamp),
                show: false,
              });
            } else {
              setTimeState({
                ...timeState,
                show: false,
              });
            }
          }}
        />
      )}
      {showContinueButton()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  dateView: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 18,
    marginVertical: 9,
  },
  flatListView: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flatList: {
    flexGrow: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  renderItemView: {
    alignItems: 'center',
    width: 40,
  },
  dateText: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 16,
    color: '#0A1F44',
  },
  dayText: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 15,
    color: '#ACB1C0',
  },
  selectText: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 17,
    alignSelf: 'center',
    color: '#FFFFFF',
  },
  unSelectText: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 17,
    alignSelf: 'center',
    color: '#ACB1C0',
  },
  selectView: {
    width: '100%',
    marginTop: 10,
    paddingVertical: 10,
    backgroundColor: '#EF4236',
    borderRadius: 6,
  },
  unSelectView: {
    width: '100%',
    marginTop: 10,
    paddingVertical: 10,
  },
  unSelectTimeView: {
    borderColor: '#BEC2CE',
    marginHorizontal: 15,
    marginVertical: 10,
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 4,
    borderWidth: 1,
  },
  selectTimeView: {
    marginHorizontal: 15,
    marginVertical: 10,
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: '#EF4236',
    borderRadius: 4,
  },
  selectTimeText: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 15,
    color: '#FFFFFF',
  },
  unSelectTimeText: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    color: '#ACB1C0',
  },
});
export default BookingLater;
