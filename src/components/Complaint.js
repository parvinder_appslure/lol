import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';
import {imageStyle, viewStyle, textStyle} from '../style/style';

const Complaint = ({navigation}) => {
  return (
    <SafeAreaView style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/Close.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>My Complaint</Text>
      <View style={styles.view_1}>
        <View style={styles.view_2}>
          <Image
            style={imageStyle.user_50x50}
            source={require('../assets/user.png')}
          />
          <View style={styles.view_4}>
            <Text style={styles.textName}>Rahul Sharma</Text>
            <Text style={styles.textAddress}>
              #12345|10 min ago | Rohini, New Delhi
            </Text>
          </View>
        </View>
        <Text style={styles.textComplaint}>
          How Difficult is it to keep our record pothole free?
        </Text>
        <Image
          style={styles.imageComplaint}
          source={require('../assets/demo.png')}
        />
        <View style={styles.view_3}>
          <Image
            style={styles.image_1}
            source={require('../assets/like.png')}
          />
          <Text style={styles.textLike}>100</Text>
          <Image
            style={styles.image_1}
            source={require('../assets/dislike.png')}
          />
          <Text style={styles.textLike}>100</Text>
          <Image
            style={styles.image_2}
            source={require('../assets/comment.png')}
          />
          <Text style={styles.textLike}>100</Text>
          <Image
            style={styles.image_3}
            source={require('../assets/comment.png')}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  view_1: {
    flex: 0,
    margin: 16,
    padding: 16,
    borderRadius: 6,
    borderColor: '#97979766',
    borderWidth: 1,
  },
  view_2: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  view_3: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
  },
  view_4: {
    marginLeft: 15,
  },
  textName: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 17,
    lineHeight: 22,
    color: '#000000',
  },
  textAddress: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 13,
    lineHeight: 18,
    color: '#000000cc',
  },
  textComplaint: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 15,
    lineHeight: 22,
    color: '#282525',
    marginVertical: 16,
  },
  textLike: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 20,
    color: '#000000',
    marginLeft: 5,
    marginRight: 25,
  },
  imageComplaint: {
    borderRadius: 4,
    alignSelf: 'center',
  },
  image_1: {
    width: 19,
    height: 14,
    resizeMode: 'contain',
  },
  image_2: {
    width: 19,
    height: 19,
    resizeMode: 'contain',
  },
  image_3: {
    width: 22,
    height: 19,
    resizeMode: 'contain',
    marginLeft: 'auto',
  },
});
export default Complaint;
