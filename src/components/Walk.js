import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Button,
  TouchableOpacity,
  ScrollView,
  StatusBar,
} from 'react-native';
import Swiper from 'react-native-swiper';

import h1 from '../assets/h1.png';
import h2 from '../assets/h2.png';
import h3 from '../assets/h3.png';
import h4 from '../assets/h4.png';
import {buttonStyle, textStyle, viewStyle} from '../style/style';

const Walk = ({navigation}) => {
  const mContent = [
    {
      title: 'Post Complaint',
      main:
        'Lorem lpsum is simply dummy text of the printing and typesetting industry,',
    },
    {
      title: 'Book Ambulance',
      main:
        'Lorem lpsum is simply dummy text of the printing and typesetting industry,',
    },
    {
      title: 'Donate Blood',
      main:
        'Lorem lpsum is simply dummy text of the printing and typesetting industry,',
    },
    {
      title: 'Make Donation',
      main:
        'Lorem lpsum is simply dummy text of the printing and typesetting industry,',
    },
  ];

  const navLogin = () => navigation.navigate('Login');
  const startButton = () => {
    return (
      <TouchableOpacity
        style={buttonStyle.submitMarginAuto}
        activeOpacity={0.5}
        onPress={navLogin}>
        <Text style={textStyle.button}>GET STARTED</Text>
      </TouchableOpacity>
    );
  };
  const swipScreen = (i, src) => {
    return (
      <View style={viewStyle.container}>
        <Text style={styles.skip} onPress={navLogin}>
          Skip
        </Text>
        <ImageBackground source={src} style={styles.image}></ImageBackground>
        <Text style={styles.title}>{mContent[i].title}</Text>
        <Text style={styles.main}>{mContent[i].main}</Text>
        {i === 3 ? startButton() : null}
      </View>
    );
  };

  return (
    <>
      <StatusBar
        barStyle="dark-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <Swiper
        showsButtons={false}
        loop={false}
        dot={<View style={styles.dot} />}
        activeDot={<View style={styles.activeDot} />}
        paginationStyle={{
          alignSelf: 'center',
          position: 'absolute',
          bottom: '15%',
        }}>
        {swipScreen(0, h1)}
        {swipScreen(1, h2)}
        {swipScreen(2, h3)}
        {swipScreen(3, h4)}
      </Swiper>
    </>
  );
};

const styles = StyleSheet.create({
  skip: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 18,
    marginTop: '10%',
    alignSelf: 'flex-end',
    marginRight: 30,
  },
  image: {
    resizeMode: 'cover',
    height: 280,
    width: 382,
    marginTop: '15%',
    alignSelf: 'center',
  },
  title: {
    fontFamily: 'Avenir',
    color: '#EF4236',
    fontWeight: '900',
    fontSize: 34,
    alignSelf: 'center',
    marginTop: '10%',
  },
  main: {
    fontFamily: 'Avenir',
    color: '#1E2432',
    fontWeight: '500',
    fontSize: 17,
    alignSelf: 'center',
    marginTop: '5%',
    marginHorizontal: 30,
    textAlign: 'center',
  },
  activeDot: {
    backgroundColor: '#EF4236',
    width: 10,
    height: 10,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4,
    marginTop: 3,
    marginBottom: 3,
  },
  dot: {
    backgroundColor: '#E1E4E8',
    width: 10,
    height: 10,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4,
    marginTop: 3,
    marginBottom: 3,
  },
});

export default Walk;
