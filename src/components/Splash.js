import React, {useEffect} from 'react';
import {StyleSheet, View, Text, Image, StatusBar} from 'react-native';
import textImage from '../assets/text-1.png';
import logoImage from '../assets/LOL-logo2.png';
import craftImage from '../assets/text.png';
import sealImage from '../assets/seal.png';
import * as actions from '../redux/actions';
import {AsyncStorageGetUser, GetProfileApi} from '../backend/Api';
import {useDispatch, useStore} from 'react-redux';

const Splash = ({navigation}) => {
  const dispatch = useDispatch();
  const store = useStore();
  const navWalk = () => navigation.replace('Walk');
  const screenHandler = async () => {
    const mUser = JSON.parse(await AsyncStorageGetUser());
    if (mUser === null) {
      navWalk();
      return;
    }
    if (mUser.id && mUser.id !== '') {
      setTimeout(async () => {
        const {deviceInfo} = store.getState();
        console.log('firebaseToken : ', deviceInfo.token);
        const {status, user} = await GetProfileApi({
          user_id: mUser.id,
          device_id: deviceInfo.id,
          device_type: deviceInfo.os,
          device_token: deviceInfo.token,
        });
        if (status) {
          dispatch(actions.Login(user));
          navigation.replace('MyDrawer');
        } else {
          navWalk();
        }
      }, 2000);
    } else {
      navWalk();
    }
  };

  useEffect(() => {
    screenHandler();
  }, []);
  return (
    <>
      <StatusBar
        barStyle="dark-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <View style={styles.container}>
        <Image source={textImage} style={styles.image_1} />
        <Image source={logoImage} style={styles.image_2} />
        <Image source={craftImage} style={styles.image_3} />
        <Image source={sealImage} style={styles.image_4} />
        <View style={styles.view_1}>
          <Text style={styles.hz1}></Text>
          <Text style={styles.hz2}></Text>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  image: {
    resizeMode: 'contain',
    width: '100%',
    height: '100%',
  },
  image_1: {
    marginTop: '15%',
    width: 344,
    height: 80,
    resizeMode: 'contain',
  },
  image_2: {
    marginTop: '10%',
    width: 180,
    height: 102,
    resizeMode: 'contain',
  },
  image_3: {
    marginTop: '25%',
    width: 238,
    height: 40,
    resizeMode: 'contain',
  },
  image_4: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
  },
  view_1: {
    height: 20,
    width: '100%',
  },
  hz1: {
    height: 10,
    width: '100%',
    backgroundColor: '#EF4236',
  },
  hz2: {
    height: 10,
    width: '100%',
    backgroundColor: '#282727',
  },
});
export default Splash;
