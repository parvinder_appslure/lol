import React from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {imageStyle, textStyle, viewStyle} from '../style/style';

const EditProfile = ({navigation}) => {
  return (
    <View style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/Close.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>Edit Profile</Text>
      <ImageBackground
        style={styles.imageBg}
        imageStyle={styles.image}
        resizeMode="cover"
        source={require('../assets/user.png')}>
        <TouchableOpacity>
          <Image
            style={styles.editImage}
            source={require('../assets/edit-1.png')}
          />
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  view_1: {
    backgroundColor: 'teal',
    alignItems: 'center',
  },
  imageBg: {
    width: 150,
    height: 150,
    alignSelf: 'center',
  },
  image: {
    borderColor: '#EF4236',
    borderWidth: 3,
    borderRadius: 75,
  },
  editImage: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
    alignSelf: 'flex-end',
  },
});
export default EditProfile;
