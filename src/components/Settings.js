import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';
import {useDispatch} from 'react-redux';
import * as actions from '../redux/actions';
import {imageStyle, viewStyle, textStyle} from '../style/style';

const Settings = ({navigation}) => {
  const dispatch = useDispatch();
  const onPressHandler = (value) => {
    if (value === 'Logout') {
      dispatch(actions.Logout());
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    } else {
      console.log(value);
      navigation.navigate(value);
    }
  };
  return (
    <SafeAreaView style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/Close.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>Settings</Text>
      <View style={styles.view}>
        <TouchableOpacity
          style={styles.subView}
          onPress={() => onPressHandler('TermCondition')}>
          <Image source={require('../assets/list.png')} style={styles.icon} />
          <Text style={styles.subTitle}>Terms & Conditions</Text>
        </TouchableOpacity>
        <View style={styles.line} />
        <TouchableOpacity
          style={styles.subView}
          onPress={() => onPressHandler('Policy')}>
          <Image
            source={require('../assets/privacy.png')}
            style={styles.icon}
          />
          <Text style={styles.subTitle}>Privacy Policy</Text>
        </TouchableOpacity>
        <View style={styles.line} />
        <TouchableOpacity
          style={styles.subView}
          onPress={() => onPressHandler('AboutUs')}>
          <Image source={require('../assets/about.png')} style={styles.icon} />
          <Text style={styles.subTitle}>About Us</Text>
        </TouchableOpacity>
        <View style={styles.line} />
        <TouchableOpacity
          style={styles.subView}
          onPress={() => onPressHandler('Logout')}>
          <Image source={require('../assets/logout.png')} style={styles.icon} />
          <Text style={styles.subTitle}>Logout</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  subTitle: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 14,
    color: '#262626',
    marginLeft: 14,
  },
  icon: {
    height: 23,
    width: 17,
    resizeMode: 'contain',
    marginLeft: 20,
  },
  view: {
    marginTop: 34,
  },
  subView: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
  },
  line: {
    backgroundColor: '#F5F5F5',
    height: 1,
    width: '100%',
  },
});
export default Settings;
