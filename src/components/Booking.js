import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import closeImage from '../assets/Close.png';
import {
  buttonStyle,
  imageStyle,
  radioStyle,
  textStyle,
  viewStyle,
} from '../style/style';

const Booking = ({navigation}) => {
  const [booking, setBooking] = useState(0);

  var radio_props = [
    {label: 'Book Now', value: 0},
    {label: 'Book for Later', value: 1},
  ];

  const navGoBack = () => navigation.goBack();
  const onRadioPress = (value) => setBooking(value);
  const continueHandler = () =>
    navigation.navigate('AmbulanceBooking', {
      bookingType: booking ? 'later' : 'now',
    });

  return (
    <>
      <SafeAreaView style={viewStyle.container}>
        <TouchableOpacity onPress={navGoBack}>
          <Image
            style={imageStyle.navigationGoBack}
            source={closeImage}></Image>
        </TouchableOpacity>
        <Text style={textStyle.title}>Choose Booking Options</Text>
        <RadioForm formHorizontal={false} animation={true}>
          {radio_props.map((obj, i) => (
            <RadioButton labelHorizontal={true} key={i}>
              <RadioButtonInput
                obj={obj}
                index={i}
                isSelected={booking === i}
                onPress={onRadioPress}
                borderWidth={1}
                buttonInnerColor={'#EF4236'}
                buttonOuterColor={booking === i ? '#EF4236' : '#0A1F44'}
                buttonSize={10}
                buttonOuterSize={20}
                buttonStyle={{}}
                buttonWrapStyle={radioStyle.buttonWrapStyle}
              />
              <RadioButtonLabel
                obj={obj}
                index={i}
                labelHorizontal={true}
                onPress={onRadioPress}
                labelStyle={radioStyle.labelStyle}
                labelWrapStyle={radioStyle.labelWrapStyle}
              />
            </RadioButton>
          ))}
        </RadioForm>
        <TouchableOpacity
          style={buttonStyle.submitMarginAuto}
          activeOpacity={0.5}
          onPress={continueHandler}>
          <Text style={textStyle.button}>CONTINUE</Text>
        </TouchableOpacity>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  text_1: {
    color: '#0A1F44',
    fontSize: 34,
    fontFamily: 'Avenir',
    lineHeight: 41,
    fontWeight: '900',
    marginTop: 30,
    marginBottom: 74,
    marginHorizontal: 30,
  },

  textInput: {
    height: 50,
    width: '82%',
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: '500',
    color: '#1E2432',
    marginTop: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    borderBottomColor: '#EAECEF',
    borderBottomWidth: 1,
  },
});

export default Booking;
