import {useIsFocused} from '@react-navigation/core';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text} from 'react-native';
import {AsyncStorageGetTime, timeFormate_mmss} from '../backend/Api';

const TimerCounter = () => {
  const isFocused = useIsFocused();
  const [state, setState] = useState({
    bookingTime: 0,
    timeLeft: 0,
  });

  const checkTimeLeft = (bookingTime) => {
    const timeleft = 600000 - (Date.now() - bookingTime);
    return timeleft > 0 ? timeleft : 0;
  };
  const updateTimeLeft = () => {
    const {bookingTime} = state;
    const timeLeft = checkTimeLeft(bookingTime);
    setState({...state, timeLeft});
  };

  useEffect(() => {
    (async () => {
      const bookingTime = Number(await AsyncStorageGetTime());
      if (bookingTime) {
        const timeLeft = checkTimeLeft(bookingTime);
        if (timeLeft > 0) {
          setState({...state, bookingTime, timeLeft});
        }
      }
    })();
  }, []);

  useEffect(() => {
    if (isFocused && state.timeLeft > 0) setTimeout(updateTimeLeft, 1000);
  }, [state.timeLeft]);
  useEffect(() => {
    if (isFocused) updateTimeLeft();
  }, [isFocused]);

  return (
    <Text style={styles.title}>
      {state.bookingTime !== 0
        ? `${timeFormate_mmss(Math.round(state.timeLeft / 1000))} mins`
        : ''}
    </Text>
  );
};
const styles = StyleSheet.create({
  title: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#767676',
  },
});
export default TimerCounter;
