import React, {useEffect, useRef, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {imageStyle, viewStyle, textStyle} from '../style/style';

const Search = ({navigation}) => {
  const [search, SetSearch] = useState('');

  useEffect(() => {
    console.log('search', search);
  }, [search]);
  return (
    <SafeAreaView style={viewStyle.container}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={imageStyle.navigationGoBack}
          source={require('../assets/close2.png')}
        />
      </TouchableOpacity>
      <Text style={textStyle.title}>Search</Text>
      <View style={styles.searchView}>
        <Image
          source={require('../assets/search2.png')}
          style={styles.searchIcon}
        />
        <TextInput
          style={styles.searchText}
          placeholder={'Search'}
          value={search}
          onChangeText={(text) => SetSearch(text)}
        />
      </View>

      <View style={styles.view_1}>
        <Text style={styles.text_1}>Recent Search</Text>
        <Text style={styles.text_2}>Clear All</Text>
      </View>

      <View style={styles.view_2}>
        <Image style={styles.image_1} source={require('../assets/user.png')} />
        <View style={styles.view_3}>
          <Text style={styles.text_3}>Raaj kumar anand</Text>
          <Text style={styles.text_4}>AAP | Patel Nagar</Text>
        </View>
        <View style={styles.view_4}>
          <TouchableOpacity style={styles.view_button}>
            <Text style={styles.text_5}>View Profile</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  searchView: {
    flex: 0,
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: '#8e8e9366',
    marginHorizontal: 30,
    alignItems: 'center',
    paddingHorizontal: 12,
    marginBottom: 33,
  },

  searchIcon: {
    height: 16,
    width: 16,
    resizeMode: 'contain',
  },
  searchText: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    color: '#000000',
    flex: 1,
    paddingLeft: 12,
  },
  view_button: {
    backgroundColor: '#EF4236',
    borderRadius: 6,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  view_1: {
    flex: 0,
    flexDirection: 'row',
    marginHorizontal: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  view_2: {
    flex: 0,
    flexDirection: 'row',
    marginTop: 20,
    height: 60,
  },
  view_3: {
    flex: 1,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#00000033',
  },
  view_4: {
    flex: 0,
    justifyContent: 'center',
    marginLeft: 'auto',
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderColor: '#00000033',
  },
  text_1: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 20,
    color: '#000000',
    lineHeight: 25,
  },
  text_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    color: '#EF4236',
    lineHeight: 20,
  },
  text_3: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 17,
    color: '#000000',
    lineHeight: 22,
  },
  text_4: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    color: '#000000',
    lineHeight: 20,
    opacity: 0.5,
  },
  text_5: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 11,
    color: '#FFFFFF',
  },
  image_1: {
    height: 60,
    width: 60,
    resizeMode: 'contain',
    marginHorizontal: 15,
  },
});
export default Search;
