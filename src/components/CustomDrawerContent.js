import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  SafeAreaView,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import {viewStyle} from '../style/style';

const CustomDrawerContent = ({navigation}) => {
  const user = useSelector((state) => state.user);
  const Data = [
    {
      id: 0,
      title: 'Home',
      value: 'Main',
      source: require('../assets/home.png'),
    },
    {
      id: 1,
      title: 'News Feed',
      value: 'NewsFeed',
      source: require('../assets/newsfeed.png'),
    },
    {
      id: 2,
      title: 'Wallet',
      value: 'Wallet',
      source: require('../assets/wallet.png'),
    },
    {
      id: 3,
      title: 'My Complaint',
      value: 'Complaint',
      source: require('../assets/complaint.png'),
    },
    {
      id: 4,
      title: 'Ambulance Booking History',
      value: 'AmbulanceBookingHistory',
      source: require('../assets/ab-history.png'),
    },
    {
      id: 5,
      title: 'Schedule Booking History',
      value: 'ScheduleBookingHistory',
      source: require('../assets/ab-history.png'),
    },
    {
      id: 6,
      title: 'Blood Donation History',
      value: 'BloodDonationHistory',
      source: require('../assets/blood-history.png'),
    },
    {
      id: 7,
      title: 'Donation History',
      value: 'DonationHistory',
      source: require('../assets/donation-history.png'),
    },
    {
      id: 8,
      title: 'Help Desk',
      value: 'HelpDesk',
      source: require('../assets/help.png'),
    },
    {
      id: 9,
      title: 'Share',
      value: 'Share',
      source: require('../assets/share-1.png'),
    },
    {
      id: 10,
      title: 'Rate LOL',
      value: 'Rate',
      source: require('../assets/rate.png'),
    },
    {
      id: 11,
      title: 'Settings',
      value: 'Settings',
      source: require('../assets/settings.png'),
    },
  ];

  const onPressHandler = (value) => {
    console.log(value);
    navigation.closeDrawer();
    switch (value) {
      case 'Main':
        navigation.navigate('Main');
        break;
      case 'NewsFeed':
        navigation.navigate('NewsFeed');
        break;
      case 'Complaint':
        navigation.navigate('Complaint');
        break;
      case 'Wallet':
        navigation.navigate('Wallet');
        break;
      case 'AmbulanceBookingHistory':
        navigation.navigate('AmbulanceBookingHistory');
        break;
      case 'ScheduleBookingHistory':
        navigation.navigate('ScheduleBookingHistory');
        break;
      case 'BloodDonationHistory':
        navigation.navigate('BloodDonationHistory');
        break;
      case 'DonationHistory':
        navigation.navigate('DonationHistory');
        break;
      case 'HelpDesk':
        navigation.navigate('HelpDesk');
        break;
      case 'Settings':
        navigation.navigate('Settings');
        break;
      default:
    }
  };
  const horizontalLine = () => <View style={styles.horizontalLine} />;
  const optionView = ({source, title, value}) => (
    <TouchableOpacity
      style={styles.controlView_2}
      onPress={() => onPressHandler(value)}>
      <Image source={source} style={styles.constrolViewImage} />
      <Text style={styles.controlViewText}>{title}</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={viewStyle.container}>
      <View style={styles.userView}>
        <View style={styles.userView_2}>
          <Image
            source={require('../assets/user.png')}
            style={styles.userImage}
          />
          <View style={styles.userEditView}>
            <Text style={styles.nameText}>{user.name}</Text>
            <Image
              source={require('../assets/editicon.png')}
              style={styles.editImage}></Image>
          </View>
        </View>
        <View style={styles.userView_3}>
          <Text style={styles.textEmail}>{user.email}</Text>
          <Text style={styles.textMobile}>{user.mobile}</Text>
        </View>
      </View>
      <View style={styles.controlView}>
        {Data.map((item) => (
          <>
            {optionView(item)}
            {horizontalLine()}
          </>
        ))}
        <Text style={styles.appVersion}>App Version 1.0.0</Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  userView: {
    flex: 0.3,
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  userView_2: {
    flex: 0.7,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: '10%',
  },
  userImage: {
    height: 70,
    width: 70,
    resizeMode: 'contain',
    borderRadius: 48,
    borderWidth: 2,
  },
  editImage: {
    width: 13,
    height: 13,
    marginTop: 10,
    marginLeft: 'auto',
    resizeMode: 'contain',
  },
  nameText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '500',
    fontSize: 15,
    color: '#FFFFFF',
  },
  userEditView: {
    marginLeft: '5%',
  },
  textEmail: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '400',
    fontSize: 15,
    color: '#FFFFFF',
  },
  textMobile: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '400',
    fontSize: 15,
    paddingTop: '2%',
    color: '#FFFFFF',
  },
  userView_3: {
    // backgroundColor: 'red',
    marginHorizontal: '10%',
  },
  controlView: {
    flex: 1,
    backgroundColor: '#EF4236',
    width: '100%',
    paddingTop: 10,
  },
  appVersion: {
    marginTop: 'auto',
    marginBottom: 8,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#FFFFFF',
    opacity: 0.5,
    alignSelf: 'center',
  },
  controlView_2: {
    marginHorizontal: '10%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
  constrolViewImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  controlViewText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '600',
    fontSize: 16,
    color: '#FFFFFF',
    marginLeft: '4%',
  },
  horizontalLine: {
    backgroundColor: '#FFFFFF',
    opacity: 0.5,
    height: 1,
    marginHorizontal: '8%',
  },
});
export default CustomDrawerContent;
