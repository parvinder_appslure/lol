import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  SafeAreaView,
  ScrollView,
  Platform,
  Alert,
} from 'react-native';
import {RegisterOtpApi} from '../backend/Api';
import closeImage from '../assets/Close.png';
import {buttonStyle, imageStyle, textStyle, viewStyle} from '../style/style';
import {useSelector} from 'react-redux';
const CreateAccount = ({navigation}) => {
  const [state, setState] = useState({
    name: '',
    email: '',
    mobile: '',
  });
  const {deviceInfo} = useSelector((state) => state);

  const navGoBack = () => navigation.goBack();
  const navLogin = () => navigation.navigate('Login');
  const navPhoneVerify = (otp, data) => {
    console.log('nav call');
    navigation.navigate('PhoneVerify', {
      type: 'signup',
      otp: otp,
      mobile: +state.mobile,
      data: data,
    });
  };
  const signUpHandler = () => {
    const userData = {
      name: state.name,
      email: state.email,
      mobile: state.mobile,
      device_id: deviceInfo.id,
      device_type: deviceInfo.os,
      device_token: deviceInfo.token,
      model_name: deviceInfo.model,
      lat: '',
      long: '',
    };
    if (state.name && state.email && state.mobile.length === 10) {
      RegisterOtpApi({mobile: +state.mobile})
        .then((data) => {
          if (data.status) {
            navPhoneVerify(data.otp, userData);
          } else {
            userExistHandler();
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      Alert.alert('incomplete Form');
    }
  };

  const userExistHandler = () => {
    Alert.alert('Sign Up Failed', 'Account already exist');
  };

  return (
    <>
      <SafeAreaView style={viewStyle.container}>
        <ScrollView>
          <TouchableOpacity onPress={navGoBack}>
            <Image style={imageStyle.navigationGoBack} source={closeImage} />
          </TouchableOpacity>
          <Text style={textStyle.title}>Create Account</Text>
          <Text style={styles.text_2}>Enter your details below</Text>
          <Text style={styles.text_3}>Name</Text>
          <TextInput
            style={styles.textInput}
            keyboardType="default"
            onChangeText={(text) => setState({...state, name: text})}
            value={state.name}
            maxLength={30}
          />
          <Text style={styles.text_3}>Email</Text>
          <TextInput
            style={styles.textInput}
            keyboardType="email-address"
            onChangeText={(text) => setState({...state, email: text})}
            value={state.email}
            maxLength={50}
          />
          <Text style={styles.text_3}>Mobile No.</Text>
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            onChangeText={(text) => setState({...state, mobile: text})}
            maxLength={10}
          />

          <TouchableOpacity
            style={buttonStyle.submit}
            activeOpacity={0.5}
            onPress={signUpHandler}>
            <Text style={textStyle.button}>Submit</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.view_1} onPress={navLogin}>
            <Text style={styles.text_4}>Already have an account? </Text>
            <Text style={styles.text_5}>Log In</Text>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  text_1: {
    color: '#0A1F44',
    fontSize: 34,
    fontFamily: 'Avenir',
    lineHeight: 41,
    fontWeight: '900',
    marginTop: 30,
    marginLeft: 30,
  },
  text_2: {
    color: '#ACB1C0',
    fontSize: 17,
    fontFamily: 'Avenir',
    lineHeight: 22,
    fontWeight: '400',
    marginTop: 9,
    marginLeft: 30,
  },
  text_3: {
    color: '#ACB1C0',
    fontSize: 16,
    fontFamily: 'Avenir',
    lineHeight: 22,
    fontWeight: '400',
    marginTop: 30,
    marginLeft: 30,
  },
  text_4: {
    color: '#0A1F44',
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 20,
  },
  text_5: {
    color: '#EF4236',
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 20,
  },
  view_1: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  textInput: {
    height: 50,
    width: '82%',
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: '500',
    color: '#1E2432',
    marginTop: 0,
    paddingHorizontal: 10,
    alignSelf: 'center',
    borderBottomColor: '#EAECEF',
    borderBottomWidth: 1,
  },
});

export default CreateAccount;
