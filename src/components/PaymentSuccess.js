import React, {useEffect} from 'react';
import {StyleSheet, SafeAreaView, Text, Image} from 'react-native';

const PaymentSuccess = ({navigation}) => {
  useEffect(() => {
    setTimeout(
      () =>
        navigation.reset({
          index: 0,
          routes: [{name: 'MyDrawer'}],
        }),
      2000,
    );
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <Image source={require('../assets/tick.png')} style={styles.image} />
      <Text style={styles.text_1}>Booking Successful</Text>
      <Text style={styles.text_2}>Your booking has been confirmed.</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 100,
    width: 100,
    resizeMode: 'cover',
  },
  text_1: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 22,
    color: '#0A1F44',
    marginVertical: 16,
  },
  text_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    color: '#000000',
  },
});

export default PaymentSuccess;
