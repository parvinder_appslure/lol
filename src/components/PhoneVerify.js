import React, {useEffect, useState} from 'react';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  SafeAreaView,
  ScrollView,
  Alert,
} from 'react-native';
import * as actions from '../redux/actions';
import {
  AsyncStorageSetUser,
  LoginOtpApi,
  RegisterOtpApi,
  SignInApi,
  SignUpApi,
} from '../backend/Api';
import closeImage from '../assets/Close.png';
import {buttonStyle, imageStyle, textStyle, viewStyle} from '../style/style';
import {useDispatch} from 'react-redux';

const PhoneVerify = ({route, navigation}) => {
  const dispatch = useDispatch();
  const [state, setState] = useState({
    userOtp: '',
    mobile: route.params.mobile,
    otp: route.params.otp,
  });
  const navGoBack = () => navigation.goBack();

  const message = () =>
    `we have sent you an SMS with a code to number +91 ${state.mobile}`;

  const onCodeChanged = (userOtp) => setState({...state, userOtp: userOtp});

  const verifiyOtpHandler = () => {
    if (+state.otp === +state.userOtp) {
      switch (route.params.type) {
        case 'signup':
          SignUpApi(route.params.data)
            .then((data) => {
              if (data.status) {
                AsyncStorageSetUser(data.user);
                dispatch(actions.Login(data.user));
                navigation.reset({
                  index: 0,
                  routes: [{name: 'MyDrawer'}],
                });
              } else {
                console.log(data);
              }
            })
            .catch((error) => console.log(error));
          break;
        case 'login':
          SignInApi({mobile: +state.mobile, otp: +state.userOtp})
            .then((data) => {
              if (data.status) {
                AsyncStorageSetUser(data.result);
                dispatch(actions.Login(data.result));
                navigation.reset({
                  index: 0,
                  routes: [{name: 'MyDrawer'}],
                });
              } else {
                console.log(data);
              }
            })
            .catch((error) => console.log(error));
          break;
        default:
      }
    } else {
      Alert.alert('Invalid otp please try again');
    }
  };
  const sendNewOtpHandler = () => {
    setState({...state, userOtp: ''});
    switch (route.params.type) {
      case 'signup':
        RegisterOtpApi({mobile: +state.mobile})
          .then((data) => {
            if (data.status) {
              setState({...state, otp: data.otp});
            } else {
              console.log(data);
            }
          })
          .catch((error) => {
            console.log('error', error);
          });
        break;
      case 'login':
        LoginOtpApi({mobile: +state.mobile})
          .then((data) => {
            if (data.status) {
              setState({...state, otp: data.otp});
            } else {
              console.log(data);
            }
          })
          .catch((error) => {
            console.log('error', error);
          });
        break;
      default:
    }
  };
  useEffect(() => {
    console.log('state', state);
  }, [state]);
  return (
    <>
      <SafeAreaView style={viewStyle.container}>
        <ScrollView>
          <TouchableOpacity onPress={navGoBack}>
            <Image
              style={imageStyle.navigationGoBack}
              source={closeImage}></Image>
          </TouchableOpacity>
          <Text style={textStyle.title}>Phone Verification</Text>
          <Text style={styles.text_2}>{message()}</Text>
          <OTPInputView
            style={styles.OTPInputView}
            pinCount={4}
            code={state.userOtp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
            onCodeChanged={onCodeChanged}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
            onCodeFilled={(code) => {
              console.log(`Code is ${code}, you are good to go!`);
            }}
          />
          <TouchableOpacity
            style={buttonStyle.submit}
            activeOpacity={0.5}
            onPress={verifiyOtpHandler}>
            <Text style={textStyle.button}>Done</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.view_1}
            activeOpacity={0.5}
            onPress={sendNewOtpHandler}>
            <Text style={styles.text_5}>Send a new code</Text>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  text_2: {
    color: '#ACB1C0',
    fontSize: 17,
    fontFamily: 'Avenir',
    lineHeight: 22,
    fontWeight: '400',
    marginTop: 9,
    marginHorizontal: 35,
  },
  text_3: {
    color: '#ACB1C0',
    fontSize: 16,
    fontFamily: 'Avenir',
    lineHeight: 22,
    fontWeight: '400',
    marginTop: 98,
    marginLeft: 30,
  },
  text_4: {
    color: '#0A1F44',
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 20,
  },
  text_5: {
    color: '#EF4236',
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 20,
  },
  view_1: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  textInput: {
    height: 50,
    width: '82%',
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: '500',
    color: '#1E2432',
    marginTop: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    borderBottomColor: '#EAECEF',
    borderBottomWidth: 1,
  },
  borderStyleBase: {
    width: 30,
    height: 45,
  },

  borderStyleHighLighted: {
    borderColor: '#03DAC6',
  },

  underlineStyleBase: {
    color: '#0A1F44',
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 36,
    width: 50,
    height: 60,
    borderWidth: 0,
    borderBottomWidth: 5,
  },

  underlineStyleHighLighted: {
    borderColor: '#cdcdcd',
  },
  OTPInputView: {
    height: 150,
    width: '80%',
    alignSelf: 'center',
  },
});

export default PhoneVerify;
