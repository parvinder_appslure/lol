import React, {useRef} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {useReduxDevToolsExtension} from '@react-navigation/devtools';
import Splash from './Splash';
import Main from './Main';
import Walk from './Walk';
import Login from './Login';
import CreateAccount from './CreateAccount';
import LoginAccount from './LoginAccount';
import PhoneVerify from './PhoneVerify';
import Booking from './Booking';
import BookingLater from './BookingLater';
import BookingSummary from './BookingSummary';
import PaymentMethod from './PaymentMethod';
import PaymentSuccess from './PaymentSuccess';
import AmbulanceBooking from './AmbulanceBooking';
import AmbulanceBookingLocation from './AmbulanceBookingLocation';
import AmbulanceBookingHistory from './AmbulanceBookingHistory';
import Settings from './Settings';
import TermCondition from './TermCondition';
import Policy from './Policy';
import AboutUs from './AboutUs';
import Search from './Search';
import Notification from './Notification';
import CustomDrawerContent from './CustomDrawerContent';
import HelpDesk from './HelpDesk';
import NewsFeed from './NewsFeed';
import Wallet from './Wallet';
import Complaint from './Complaint';
import BloodDonationHistory from './BloodDonationHistory';
import DonationHistory from './DonationHistory';
import SomeoneBooking from './SomeoneBooking';
import PaymentOption from './PaymentOption';
import BookingTimer from './BookingTimer';
import AmbulanceDetails from './AmbulanceDetails';
import ScheduleBookingHistory from './ScheduleBookingHistory';

const Drawer = createDrawerNavigator();
const MyDrawer = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Main"
      drawerContent={(props) => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="Main" component={Main} />
    </Drawer.Navigator>
  );
};
const Stack = createStackNavigator();
const Navigator = () => {
  const navigationRef = useRef();
  useReduxDevToolsExtension(navigationRef);

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        initialRouteName="Splash">
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Walk"
          component={Walk}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LoginAccount"
          component={LoginAccount}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CreateAccount"
          component={CreateAccount}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Booking"
          component={Booking}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="BookingLater"
          component={BookingLater}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="BookingTimer"
          component={BookingTimer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="BookingSummary"
          component={BookingSummary}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PaymentOption"
          component={PaymentOption}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PaymentMethod"
          component={PaymentMethod}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PaymentSuccess"
          component={PaymentSuccess}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AmbulanceBooking"
          component={AmbulanceBooking}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AmbulanceDetails"
          component={AmbulanceDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SomeoneBooking"
          component={SomeoneBooking}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AmbulanceBookingLocation"
          component={AmbulanceBookingLocation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Settings"
          component={Settings}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TermCondition"
          component={TermCondition}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Policy"
          component={Policy}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AboutUs"
          component={AboutUs}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PhoneVerify"
          component={PhoneVerify}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Search"
          component={Search}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Notification"
          component={Notification}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="NewsFeed"
          component={NewsFeed}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Wallet"
          component={Wallet}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Complaint"
          component={Complaint}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AmbulanceBookingHistory"
          component={AmbulanceBookingHistory}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ScheduleBookingHistory"
          component={ScheduleBookingHistory}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="BloodDonationHistory"
          component={BloodDonationHistory}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="DonationHistory"
          component={DonationHistory}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="HelpDesk"
          component={HelpDesk}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyDrawer"
          component={MyDrawer}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default Navigator;
