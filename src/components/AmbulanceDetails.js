import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  SafeAreaView,
  View,
  Dimensions,
  Linking,
} from 'react-native';
import {
  BookingStatusForHomeApi,
  LatitudeDelta,
  LongitudeDelta,
} from '../backend/Api';

import MapView, {Marker} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import TextTicker from 'react-native-text-ticker';

import {GOOGLE_MAPS_APIKEY} from '../backend/Config';
import originImage from '../assets/origin.png';
import destinationImage from '../assets/destination.png';
import {imageStyle, textStyle, viewStyle} from '../style/style';
import {useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
const {width, height} = Dimensions.get('window');

const AmbulanceDetails = ({route, navigation}) => {
  const {coords, user, netInfo} = useSelector((state) => state);
  const isFocused = useIsFocused();
  const [region, setRegion] = useState({
    latitude: coords.latitude,
    longitude: coords.longitude,
    latitudeDelta: LatitudeDelta,
    longitudeDelta: LongitudeDelta(),
  });
  const [driverCoord, setDriverCoord] = useState({
    latitude: 0,
    longitude: 0,
  });

  const [sourceCoord, setSourceCoord] = useState({
    latitude: +route.params.source_lat,
    longitude: +route.params.source_long,
  });
  const [destinationCoord, setDestinationCoord] = useState({
    latitude: +route.params.destination_lat,
    longitude: +route.params.destination_long,
  });

  const showTitle = () => (
    <Text style={textStyle.title}>Ambulance Details</Text>
  );
  const showBackButton = () => (
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <Image
        style={imageStyle.navigationGoBack}
        source={require('../assets/Close.png')}></Image>
    </TouchableOpacity>
  );
  const showMapView = () => (
    <MapView
      style={styles.map}
      initialRegion={region}
      showsUserLocation={true}
      showsMyLocationButton={false}
      pitchEnabled={true}
      rotateEnabled={true}
      zoomEnabled={true}
      scrollEnabled={true}
      onRegionChangeComplete={(region) => setRegion(region)}
      onPress={(event) => console.log(event.nativeEvent.coordinate)}>
      {showMarker(sourceCoord, originImage)}
      {showMarker(destinationCoord, destinationImage)}
      {showDirection()}
    </MapView>
  );
  const showMarker = (coordinate, source) =>
    coordinate.latitude !== 0 &&
    coordinate.longitude !== 0 && (
      <Marker coordinate={coordinate}>
        <Image source={source} style={styles.markerIcon} />
      </Marker>
    );
  const showDirection = () =>
    ![
      sourceCoord.latitude,
      sourceCoord.longitude,
      destinationCoord.latitude,
      destinationCoord.latitude,
    ].includes(0) && (
      <MapViewDirections
        origin={sourceCoord}
        destination={destinationCoord}
        strokeWidth={3}
        strokeColor="#F76B1C"
        apikey={GOOGLE_MAPS_APIKEY}
        onReady={(result) => console.log(result.distance)}
      />
    );
  const showRouteView = () =>
    route.params.source_address !== '' &&
    route.params.destination_address !== '' && (
      <View style={styles.coordsSelectView}>
        <View style={styles.CSV_1}>
          <Image
            source={require('../assets/origin2.png')}
            style={styles.imageOrigin}
          />
          <Image
            source={require('../assets/coordsLine.png')}
            style={styles.imageCordsLine}
          />
          <Image
            source={require('../assets/destination2.png')}
            style={styles.imageDestination}
          />
        </View>
        <View style={styles.CSV_2}>
          <View style={styles.pickupView}>
            <Text style={styles.textLocation}>Pickup Location</Text>
            <TextTicker
              style={styles.textPickup}
              duration={8000}
              loop
              bounce
              repeatSpacer={50}
              marqueeDelay={3000}>
              {route.params.source_address}
            </TextTicker>
          </View>
          <View style={styles.horizontalLine}></View>
          <View style={styles.dropView}>
            <Text style={styles.textLocation}>Drop Location</Text>
            <TextTicker
              style={styles.textDrop}
              duration={8000}
              loop
              bounce
              repeatSpacer={50}
              marqueeDelay={3000}>
              {route.params.destination_address}
            </TextTicker>
          </View>
        </View>
      </View>
    );
  const showDriverView = () => {
    const {
      driver_name,
      vehicle_number,
      driver_image,
      driver_mobile,
    } = route.params;
    return (
      <View style={styles.dr_container}>
        <View style={styles.dr_view_1}>
          <Image
            style={styles.dr_driverImage}
            // source={{ uri: driver_image }}
            source={require('../assets/user.png')}
          />
          <Text style={styles.dr_driverName}>{driver_name}</Text>
          <TouchableOpacity
            style={styles.dr_callTouch}
            onPress={() => {
              const url = 'tel:' + driver_mobile;
              Linking.openURL(url);
            }}>
            <Image
              style={styles.dr_callImage}
              source={require('../assets/Call.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.dr_view_2}>
          <Image
            style={styles.dr_ambImage}
            source={require('../assets/ambulnce.png')}
          />
          <View style={styles.dr_ambView}>
            <Text style={styles.dr_ambType}>Normal Ambulance</Text>
            <Text style={styles.dr_ambNumber}>{vehicle_number}</Text>
          </View>
          {/* <Text style={styles.dr_ambReaching}>{`Reaching\nin\n10\nmins`}</Text> */}
        </View>
      </View>
    );
  };
  useEffect(() => {
    let timer;
    const callback = async () => {
      if (netInfo.isInternetReachable) {
        const {
          status = false,
          ambulance_booking_flag = 0,
          details = {},
        } = await BookingStatusForHomeApi({user_id: user.id});
        if (status) {
          if (ambulance_booking_flag === 2) {
            if (Object.keys(details).length !== 0) {
              const {driver_latitude = 0, driver_longitude = 0} = details;
              setDriverCoord({
                latitude: +driver_latitude,
                longitude: +driver_longitude,
              });
            }
          }
        } else {
          navigation.goBack();
        }
      }
      timer = setTimeout(callback, 5000);
    };
    if (isFocused) {
      console.log('focused');
      callback();
    } else {
      console.log('unfocused');
      clearTimeout(timer);
    }
    return () => {
      console.log('details unmount');
      clearTimeout(timer);
    };
  }, [isFocused]);

  useEffect(() => {
    console.log('driver', driverCoord);
    const {latitude, longitude} = driverCoord;
    if (
      (latitude !== sourceCoord.latitude ||
        longitude !== sourceCoord.longitude) &&
      latitude !== 0 &&
      longitude !== 0
    ) {
      setSourceCoord({latitude, longitude});
      console.log('update');
    }
  }, [driverCoord]);

  return (
    <SafeAreaView style={viewStyle.container}>
      {showBackButton()}
      {showTitle()}
      <View style={styles.mapView}>
        {showMapView()}
        {showRouteView()}
        {showDriverView()}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mapView: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
  coordsSelectView: {
    position: 'absolute',
    top: '1%',
    width: '85%',
    alignSelf: 'center',
    borderRadius: 16,
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'row',
  },
  CSV_1: {
    flex: 0.25,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingVertical: '5%',
  },
  imageOrigin: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },
  imageCordsLine: {
    width: 2,
    resizeMode: 'cover',
  },
  imageDestination: {
    width: 16,
    height: 22,
    resizeMode: 'contain',
  },
  CSV_2: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontalLine: {
    width: '100%',
    height: 1,
    backgroundColor: '#EFEFEF',
  },
  pickupView: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 16,
  },
  textPickup: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 17,
    color: '#242E42',
  },
  dropView: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 16,
  },
  textDrop: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 17,
    color: '#242E42',
  },

  textLocation: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 13,
    color: '#ABAFBD',
  },
  markerIcon: {width: 40, height: 40, resizeMode: 'contain'},

  dr_container: {
    position: 'absolute',
    backgroundColor: 'white',
    bottom: 20,
    width: '90%',
    alignSelf: 'center',
    paddingVertical: 10,
    borderRadius: 15,
  },
  dr_view_1: {
    flexDirection: 'row',
    borderBottomColor: '#EFEFF4',
    borderBottomWidth: 1,
    paddingBottom: 10,
    alignItems: 'center',
  },
  dr_view_2: {
    flexDirection: 'row',
    paddingTop: 10,
    alignItems: 'center',
  },
  dr_driverName: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 17,
    color: '#242E42',
  },
  dr_ambView: {},
  dr_ambNumber: {
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 15,
    color: '#000000',
    marginTop: 5,
  },
  dr_ambType: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 17,
    color: '#242E42',
  },
  dr_ambReaching: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#EF4236',
    marginLeft: 'auto',
    textAlign: 'center',
    marginHorizontal: 20,
  },
  dr_driverImage: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
    borderRadius: 25,
    marginHorizontal: 20,
  },
  dr_ambImage: {
    height: 60,
    width: 60,
    // resizeMode: 'contain',
    borderRadius: 25,
    marginHorizontal: 20,
  },
  dr_callImage: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
    borderRadius: 25,
  },
  dr_callTouch: {
    marginLeft: 'auto',
    marginHorizontal: 20,
  },
});

export default AmbulanceDetails;
