import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Text,
  View,
  ScrollView,
  TextInput,
  Modal,
} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {useDispatch, useSelector} from 'react-redux';
import * as actions from '../redux/actions';
import {
  CouponApi,
  BookingApi,
  AsyncStorageSetBookingId,
  textInPrice,
} from '../backend/Api';
import {
  buttonStyle,
  imageStyle,
  radioStyle,
  textStyle,
  viewStyle,
} from '../style/style';
import Loader from './Loader';

const PaymentOption = ({navigation, route}) => {
  const dispatch = useDispatch();

  const {user, location} = useSelector((state) => state);
  const [paymentState, setPaymentState] = useState({
    value: 0,
    props: [
      {label: 'Online', value: 0},
      {label: 'Cash', value: 1},
    ],
  });
  const [stateParam, setStateParam] = useState({
    ...route.params,
  });

  const [walletState, setWalletState] = useState({
    show: false,
  });
  const [couponState, setCouponState] = useState({id: '', code: '', input: ''});
  const [isLoading, setIsLoading] = useState(false);
  const onRadioPress = (value) => setPaymentState({...paymentState, value});
  const showBackButton = () => (
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <Image
        style={imageStyle.navigationGoBack}
        source={require('../assets/Close.png')}
      />
    </TouchableOpacity>
  );
  const showTitle = () => (
    <Text style={textStyle.title}>Choose Payment Options</Text>
  );
  const showSubTitle = (title) => (
    <View style={styles.paymentView}>
      <Text style={styles.paymentText}>{title}</Text>
    </View>
  );
  const showPaymentMethod = () => (
    <RadioForm formHorizontal={false} animation={true}>
      {paymentState.props.map((obj, i) => (
        <RadioButton labelHorizontal={true} key={i}>
          <RadioButtonInput
            obj={obj}
            index={i}
            isSelected={paymentState.value === i}
            onPress={onRadioPress}
            borderWidth={1}
            buttonInnerColor={'#EF4236'}
            buttonOuterColor={paymentState.value === i ? '#EF4236' : '#0A1F44'}
            buttonSize={10}
            buttonOuterSize={20}
            buttonStyle={{}}
            buttonWrapStyle={radioStyle.buttonWrapStyle}
          />
          <RadioButtonLabel
            obj={obj}
            index={i}
            labelHorizontal={true}
            onPress={onRadioPress}
            labelStyle={radioStyle.labelStyle}
            labelWrapStyle={radioStyle.labelWrapStyle}
          />
        </RadioButton>
      ))}
    </RadioForm>
  );
  const showCoupon = () => (
    <View style={styles.cp_view}>
      {couponState.code === '' && (
        <>
          <TextInput
            style={styles.cp_input}
            value={couponState.input}
            onChangeText={(value) =>
              setCouponState({...couponState, input: value})
            }
          />
          <TouchableOpacity style={styles.cp_touch} onPress={couponHandler}>
            <Text style={styles.cp_text}>APPLY</Text>
          </TouchableOpacity>
        </>
      )}
      {couponState.code !== '' && (
        <>
          <Text
            style={[styles.cp_input, {color: '#16b922'}]}>{`AMB Applied`}</Text>
          <TouchableOpacity
            style={styles.cp_touch}
            onPress={() => setCouponState({id: '', code: '', input: ''})}>
            <Image
              source={require('../assets/close2.png')}
              style={styles.cp_closeImage}
            />
          </TouchableOpacity>
        </>
      )}
    </View>
  );
  const showButton = () => (
    <TouchableOpacity
      style={buttonStyle.submit}
      activeOpacity={0.5}
      // onPress={() => navigation.navigate('PaymentSuccess')}
      onPress={buttonHandler}>
      <Text style={textStyle.button}>CONTINUE</Text>
    </TouchableOpacity>
  );
  const fareField = (data, title, key, prefix, sufix) => (
    <View style={styles.fare_titleView}>
      <View style={styles.fare_titleDes}>
        <Text style={styles.fare_subTitleText}>{title}</Text>
      </View>
      <View style={styles.fare_titleSize}>
        <Text style={styles.fare_subTitleText2}>{`${prefix} ${Math.floor(
          key !== 'igst_price' ? data[key] : data.cgst_price + data.sgst_price,
        )} ${sufix}`}</Text>
      </View>
    </View>
  );
  const showEstimateFare = () => (
    <View style={styles.fare_view}>
      <View style={styles.fare_titleView}>
        <View style={styles.fare_titleDes}>
          <Text style={styles.fare_titleText}>Description</Text>
        </View>
        <View style={styles.fare_titleSize}>
          <Text style={styles.fare_titleText}>Charges</Text>
        </View>
      </View>
      {fareField(stateParam.result, 'Base Fare', 'base_price', '\u20B9', '')}
      {fareField(
        stateParam.result,
        'Booking Fee',
        'ambulence_booking_fee',
        '\u20B9',
        '',
      )}
      {fareField(stateParam.result, 'Time Fare', 'time_price', '\u20B9', '')}
      {fareField(stateParam.result, 'KM Fare', 'km_price', '\u20B9', '')}
      {fareField(stateParam.result, 'IGST Fare', 'igst_price', '\u20B9', '')}
      {/* {fareField(stateParam.result, 'CGST Fare', 'cgst_price', '\u20B9', '')} */}

      {stateParam.covid &&
        fareField(
          stateParam.result,
          'Covid Fare',
          'extras_for_covids',
          '\u20B9',
          '',
        )}

      {fareField(
        stateParam.result,
        'Estimate Fare',
        'estimates_prices',
        '\u20B9',
        '',
      )}
    </View>
  );
  const showFareCalculator = () => {
    return (
      <View style={styles.fare_view}>
        <View style={styles.fare_titleView}>
          <View style={styles.fare_titleDes}>
            <Text style={styles.fare_titleText}>Description</Text>
          </View>
          <View style={styles.fare_titleSize}>
            <Text style={styles.fare_titleText}>Charges</Text>
          </View>
        </View>
        {fareField(
          stateParam.ambulance,
          'Base Fare',
          'base_fare',
          '\u20B9',
          '',
        )}
        {fareField(
          stateParam.ambulance,
          'Including KM',
          'base_running_km',
          '',
          'km',
        )}
        {fareField(
          stateParam.ambulance,
          'Including Time',
          'base_waiting_time',
          '',
          'mins',
        )}
        {fareField(stateParam.ambulance, 'Per KM', 'per_km', '\u20B9 ', '')}
        {fareField(
          stateParam.ambulance,
          'Per Minute',
          'waiting_charges',
          '\u20B9 ',
          '',
        )}
        {stateParam.covid &&
          fareField(
            stateParam.ambulance,
            'For Covid',
            'covid_ambulance',
            '\u20B9 ',
            '',
          )}
      </View>
    );
  };
  const bookingHandler = async () => {
    const {
      distance,
      ambulance,
      originCoords,
      destinationCoords,
      covid,
      navigate,
      bookingFor,
      schedule,
      wallet_amount,
    } = stateParam;
    const type = {
      PaymentOption: 'book_now',
      BookingLater: 'schedule',
    };

    const {estimates_prices} = stateParam.result;
    const body = {
      ambulence_type_id: ambulance.id.toString(),
      user_id: user.id,
      source_address: location.pickup,
      source_lat: originCoords.latitude,
      source_long: originCoords.longitude,
      destination_address: location.drop,
      destination_lat: destinationCoords.latitude,
      destination_long: destinationCoords.longitude,
      distance: distance,
      is_covid_booking: covid ? '1' : '0',
      method_of_payment: paymentState.value === 0 ? 'online' : 'cash',
      type: type[navigate],
      schedule_date: schedule.date,
      schedule_time: schedule.time,
      booking_for: bookingFor.type,
      booking_for_mobile: bookingFor.mobile,
      coupan_code: couponState.code,
      coupan_code_id: couponState.id,
      wallet_amount: estimates_prices,
    };
    setIsLoading(true);
    const res = await BookingApi(body);
    setIsLoading(false);
    if (res.status) {
      await AsyncStorageSetBookingId(res.id.toString());
      if (body.type === 'book_now') {
        updateWalletAmount();
        navigation.navigate('BookingTimer', {
          id: res.id,
          user_id: user.id,
        });
      } else {
        updateWalletAmount();
        navigation.navigate('PaymentSuccess');
      }
    } else {
      alert('Something went wrong Please try again');
      navigation.reset({
        index: 0,
        routes: [{name: 'MyDrawer'}],
      });
    }
  };
  const updateWalletAmount = () => {
    const amount = +user.wallet_amount;
    const {estimates_prices} = stateParam.result;
    dispatch(actions.WalletUpdate(amount - estimates_prices));
  };
  const buttonHandler = async () => {
    const amount = +user.wallet_amount;
    const {estimates_prices} = stateParam.result;
    if (amount >= +estimates_prices) {
      bookingHandler();
    } else {
      setWalletState({...walletState, show: true});
    }
  };
  const couponHandler = async () => {
    const input = couponState.input;
    if (input) {
      const body = {
        module: '1',
        coupan_code: input,
      };
      setIsLoading(true);
      const res = await CouponApi(body);
      setIsLoading(false);
      if (res.status) {
        setCouponState({id: res.coupan_id, code: input, input: ''});
        alert('Coupon Applied');
      } else {
        setCouponState({id: '', code: '', input: ''});
        alert('Invalid Coupon');
      }
    }
  };
  const showWallet = () => (
    <Modal
      animationType="slide"
      transparent={true}
      visible={walletState.show}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={modalStyle.container}>
        <View style={modalStyle.modalView}>
          <Image
            source={require('../assets/covid.png')}
            style={modalStyle.image}
          />
          <Text style={modalStyle.title}>Low Wallet Balance!</Text>
          <Text
            style={
              modalStyle.subTitle
            }>{`Available Balance \u20B9${user.wallet_amount}`}</Text>
          <Text style={modalStyle.subTitle_2}>
            {`Minimum Wallet Balance require to book ambulance \u20B9${
              stateParam.result.estimates_prices
            }. Please recharge your wallet with alteast \u20B9${
              stateParam.result.estimates_prices - user.wallet_amount
            }.`}
          </Text>
          <View style={modalStyle.wallet_view}>
            <TouchableOpacity
              onPress={() => setWalletState({...walletState, show: false})}>
              <Text style={modalStyle.wallet_cancel}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setWalletState({...walletState, show: false});
                navigation.navigate(
                  'Wallet',
                  stateParam.result.estimates_prices - user.wallet_amount,
                );
              }}>
              <Text style={modalStyle.wallet_add}>Add Money</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );

  return (
    <SafeAreaView style={viewStyle.container}>
      {isLoading && <Loader />}
      <ScrollView>
        {showBackButton()}
        {showTitle()}
        <Text style={styles.amountText}>
          {textInPrice(stateParam.result.estimates_prices)}
        </Text>
        <Text style={styles.amountText_2}>AMOUNT TO PAY</Text>

        {showSubTitle('Select Payment Method')}
        {showPaymentMethod()}
        {showSubTitle('Apply Coupon')}
        {showCoupon()}
        {showSubTitle('Fare Calculator')}
        {showFareCalculator()}
        {showSubTitle('Estimate Fare')}
        {showEstimateFare()}
        {showButton()}
        {showWallet()}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  amountText: {
    fontFamily: 'Ping Fang SC',
    fontWeight: '600',
    fontSize: 50,
    color: '#0A1F44',
    alignSelf: 'center',
    marginTop: 10,
  },
  amountText_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 13,
    color: '#000000',
    opacity: 0.5,
    alignSelf: 'center',
  },
  paymentView: {
    marginTop: 20,
    paddingVertical: 12,
    paddingHorizontal: 20,
    backgroundColor: '#F7F7F7',
  },
  paymentText: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 16,
    color: '#0A1F44',
  },
  cp_view: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 8,
    margin: 20,
    marginBottom: 0,
    alignItems: 'center',
  },
  cp_input: {
    flex: 1,
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: 'black',
    paddingHorizontal: 20,
  },
  cp_touch: {
    padding: 10,
  },
  cp_closeImage: {
    height: 15,
    width: 15,
    resizeMode: 'contain',
    marginRight: 15,
  },
  cp_text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#EF4236',
  },
  fare_view: {
    margin: 20,
  },
  fare_titleView: {
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 20,
  },
  fare_titleDes: {
    flex: 0.5,
  },
  fare_titleSize: {
    flex: 0.5,
    alignItems: 'flex-end',
  },
  fare_titleText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: '#0A1F44',
  },
  fare_subTitleText: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 15,
    color: '#282727',
  },
  fare_subTitleText2: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 15,
    color: '#EF4236',
  },
});

const modalStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00000099',
    justifyContent: 'center',
  },
  modalView: {
    borderRadius: 8,
    backgroundColor: 'white',
    marginHorizontal: 30,
    paddingVertical: 20,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#0A1F44',
    alignSelf: 'center',
  },
  subTitle: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 15,
    color: '#000000',
    alignSelf: 'center',
    marginTop: 5,
  },
  subTitle_2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 14,
    color: '#00000080',
    alignSelf: 'center',
    marginVertical: 5,
    marginHorizontal: 30,
    textAlign: 'center',
  },
  wallet_view: {
    marginTop: 5,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  wallet_cancel: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#0A1F4480',
  },
  wallet_add: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#EF4236',
  },
  image: {
    height: 70,
    width: 70,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginBottom: 15,
  },
  touch: {
    backgroundColor: '#EF4236',
    borderRadius: 8,
    width: 100,
    paddingVertical: 10,
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  touchText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 14,
    color: '#FFFFFF',
  },
});

export default PaymentOption;
