import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  SafeAreaView,
  View,
  Text,
} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {GOOGLE_MAPS_APIKEY} from '../backend/Config';
import {viewStyle} from '../style/style';

const AmbulanceBookingLocation = ({route, navigation}) => {
  const onPressHandler = (data, details = null) => {
    navigation.navigate('AmbulanceBooking', {
      location: details.geometry.location,
      locationType: route.params.locationType,
      description: data.description,
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.headerTouch}>
          <Image
            style={styles.headerImage}
            source={require('../assets/Close.png')}
          />
        </TouchableOpacity>
        <Text style={styles.title}>{route.params.locationType}</Text>
      </View>
      <GooglePlacesAutocomplete
        placeholder="Search"
        fetchDetails={true}
        onPress={onPressHandler}
        query={{
          key: GOOGLE_MAPS_APIKEY,
          language: 'en',
          location: `${route.params.region.latitude},${route.params.region.longitude}`,
          radius: '150000',
        }}
        onFail={(error) => console.log(error)}
        styles={{
          container: {
            marginHorizontal: 20,
          },
          textInputContainer: {
            overflow: 'hidden',
            paddingHorizontal: 10,
            borderRadius: 15,
            borderColor: '#242E4280',
            borderWidth: 1,
          },
          textInput: {
            color: '#5d5d5d',
            fontSize: 16,
          },
          predefinedPlacesDescription: {
            color: '#1faadb',
          },
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    ...viewStyle.container,
    ...viewStyle.statusBarPadding,
  },
  header: {
    justifyContent: 'center',
    marginBottom: 30,
  },
  headerTouch: {
    position: 'absolute',
    paddingHorizontal: 20,
  },
  headerImage: {
    width: 18,
    height: 16,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '500',
    fontSize: 30,
    color: '#0A1F44',
    alignSelf: 'center',
    textTransform: 'capitalize',
  },
});
export default AmbulanceBookingLocation;
