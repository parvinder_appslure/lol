import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
  ScrollView,
  Linking,
} from 'react-native';
import {useSelector} from 'react-redux';

import moment from 'moment';
import {ScheduleBookingHistoryApi, textInPrice} from '../backend/Api';
import {imageStyle, textStyle, viewStyle} from '../style/style';
import Loader from './Loader';

const ScheduleBookingHistory = ({navigation, route}) => {
  const {user} = useSelector((state) => state);
  const [state, setState] = useState({
    lists: [],
    isLoading: false,
  });

  const HistoryView = () =>
    state.lists.map(
      ({
        request_id,
        source_address,
        destination_address,
        schedule_time,
        schedule_date,
        booking_status,
      }) => (
        <View style={styles.hs_view} key={`bid_${request_id}`}>
          <View style={styles.hs_statusView}>
            <Text style={styles.hs_statusText}>{booking_status}</Text>
          </View>
          <View style={styles.hs_view_1}>
            <Image
              source={require('../assets/ambulane-icons.png')}
              style={styles.hs_ambImage}
            />
            <View style={styles.hs_view_11}>
              <Text style={styles.hs_textTime}>Schedule Booking</Text>
              <Text style={styles.hs_textTime}>
                {moment(
                  `${schedule_date} ${schedule_time}`,
                  'YYYY-MM-DD HH:mm',
                ).format('DD-MM-YYYY, hh:mm a')}
              </Text>
              <Text
                style={
                  styles.hs_textBooking
                }>{`Request Id #${request_id}`}</Text>
            </View>
          </View>
          <View style={styles.hs_view_2} />
          <View style={styles.hs_view_21}>
            <View style={styles.hs_view_211}>
              <Image
                style={styles.hs_dotImage}
                source={require('../assets/origin2.png')}
              />
              <Image
                style={styles.hs_barImage}
                source={require('../assets/coordsLine.png')}
              />
              <Image
                style={styles.hs_dotImage}
                source={require('../assets/origin2.png')}
              />
            </View>
            <View style={styles.hs_view_212}>
              <Text style={styles.hs_textSource}>{source_address}</Text>
              <Text style={styles.hs_textSource}>{destination_address}</Text>
            </View>
          </View>
        </View>
      ),
    );
  useEffect(() => {
    (async () => {
      const {id} = user;
      const {status = false, lists} = await ScheduleBookingHistoryApi({
        user_id: id,
      });
      if (status) {
        setState({...state, lists, isLoading: false});
        console.log(JSON.stringify(lists, null, 2));
      } else {
        setState({...state, isLoading: false});
      }
    })();
  }, []);

  return (
    <SafeAreaView style={[viewStyle.container, {backgroundColor: 'white'}]}>
      {state.isLoading && <Loader />}
      <ScrollView>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image
            style={imageStyle.navigationGoBack}
            source={require('../assets/Close.png')}
          />
        </TouchableOpacity>
        <Text style={textStyle.title}>Schedule Booking History</Text>
        {!state.isLoading && <HistoryView />}
      </ScrollView>
    </SafeAreaView>
  );
};

export default ScheduleBookingHistory;

const styles = StyleSheet.create({
  hs_view: {
    borderRadius: 6,
    marginHorizontal: 16,
    backgroundColor: 'white',
    marginBottom: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  hs_statusView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingTop: 5,
  },
  hs_statusText: {
    padding: 10,
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    lineHeight: 18,
    color: '#1E2432',
    textTransform: 'capitalize',
  },
  hs_invoiceText: {
    padding: 10,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 18,
    color: '#000000',
    opacity: 0.5,
    textTransform: 'capitalize',
  },
  hs_ambImage: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },
  hs_view_1: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
  hs_view_11: {
    marginLeft: 10,
  },
  hs_textTime: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 13,
    lineHeight: 18,
    color: '#1E2432',
  },
  hs_textPrice: {
    marginLeft: 'auto',
    fontFamily: 'Ping Fang SC',
    fontWeight: '600',
    fontSize: 15,
    color: '#EF4236',
    lineHeight: 22,
  },
  hs_textBooking: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 13,
    color: '#000000',
    opacity: 0.4,
  },
  hs_view_2: {
    width: '100%',
    backgroundColor: '#F1F2F6',
    height: 1,
  },
  hs_view_21: {
    padding: 10,
    flex: 0,
    flexDirection: 'row',
  },
  hs_view_211: {
    flex: 0.12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  hs_view_212: {
    flex: 1,
    justifyContent: 'space-around',
  },
  hs_textSource: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 14,
    color: '#242E42',
  },
  hs_dotImage: {
    width: 11,
    height: 11,
    resizeMode: 'contain',
  },
  hs_barImage: {
    height: 20,
    width: 2,
    resizeMode: 'contain',
    marginVertical: 2,
  },
});
