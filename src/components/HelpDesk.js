import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {buttonStyle, imageStyle, textStyle, viewStyle} from '../style/style';

const HelpDesk = ({navigation}) => {
  return (
    <SafeAreaView style={viewStyle.container}>
      <ScrollView>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image
            style={imageStyle.navigationGoBack}
            source={require('../assets/Close.png')}
          />
        </TouchableOpacity>
        <Text style={textStyle.title}>Help Desk</Text>
        <Image
          source={require('../assets/support.png')}
          style={styles.supportImage}
        />
        <Text style={styles.helpText}>Need Some Help?</Text>
        <Text style={styles.helpText2}>
          Please give us in between 12 to 24 working hours to address your
          issues.
        </Text>
        <Text style={styles.text_3}>Name</Text>
        <TextInput
          style={styles.textInput}
          keyboardType="default"
          // onChangeText={(text) => setState({...state, name: text})}
          // value={state.name}
          maxLength={30}
        />
        <Text style={styles.text_3}>Email</Text>
        <TextInput
          style={styles.textInput}
          keyboardType="email-address"
          // onChangeText={(text) => setState({...state, email: text})}
          // value={state.email}
          maxLength={50}
        />
        <Text style={styles.text_3}>Mobile No.</Text>
        <TextInput
          style={styles.textInput}
          keyboardType="numeric"
          // onChangeText={(text) => setState({...state, mobile: text})}
          maxLength={10}
        />
        <Text style={styles.text_3}>Message</Text>
        <TextInput
          style={[styles.textInput, {height: 120}]}
          keyboardType="default"
          // onChangeText={(text) => setState({...state, name: text})}
          // value={state.name}
          maxLength={200}
        />
        <TouchableOpacity
          style={buttonStyle.submit}
          activeOpacity={0.5}
          // onPress={signUpHandler}
        >
          <Text style={textStyle.button}>Send</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  supportImage: {
    height: 150,
    width: 150,
    resizeMode: 'cover',
    alignSelf: 'center',
    marginTop: 20,
  },
  helpText: {
    fontFamily: 'Avenir',
    fontWeight: '900',
    fontSize: 20,
    color: '#1E2432',
    alignSelf: 'center',
    marginTop: 3,
  },
  helpText2: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 14,
    color: '#7D7D7E',
    textAlign: 'center',
    marginHorizontal: 50,
    marginTop: 9,
  },
  text_3: {
    color: '#ACB1C0',
    fontSize: 16,
    fontFamily: 'Avenir',
    lineHeight: 22,
    fontWeight: '400',
    marginTop: 30,
    marginLeft: 30,
  },
  textInput: {
    height: 50,
    width: '82%',
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: '500',
    color: '#1E2432',
    marginTop: 0,
    paddingHorizontal: 10,
    alignSelf: 'center',
    borderBottomColor: '#EAECEF',
    borderBottomWidth: 1,
  },
});

export default HelpDesk;
