import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {useSelector} from 'react-redux';

import closeImage from '../assets/Close.png';
import {LoginOtpApi} from '../backend/Api';
import {buttonStyle, imageStyle, textStyle, viewStyle} from '../style/style';
import Loader from './Loader';

const LoginAccount = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [mobile, setMobile] = useState('');
  const {deviceInfo} = useSelector((state) => state);
  const navGoBack = () => navigation.goBack();
  const navCreateAccount = () => navigation.navigate('CreateAccount');

  const loginHandler = () => {
    if (mobile.length === 10) {
      setIsLoading(true);
      LoginOtpApi({
        mobile: +mobile,
        device_id: deviceInfo.id,
        device_type: deviceInfo.os,
        device_token: deviceInfo.token,
        model_name: deviceInfo.model,
      })
        .then((data) => {
          setIsLoading(false);
          if (data.status) {
            navigation.navigate('PhoneVerify', {
              type: 'login',
              otp: +data.otp,
              mobile: +mobile,
            });
          } else {
            console.log(data);
          }
        })
        .catch((error) => {
          console.log('error', error);
        });
    } else {
      alert('Enter Valid Mobile Number');
    }
  };

  if (isLoading) {
    return <Loader />;
  } else {
    return (
      <>
        <SafeAreaView style={viewStyle.container}>
          <ScrollView>
            <TouchableOpacity onPress={navGoBack}>
              <Image style={imageStyle.navigationGoBack} source={closeImage} />
            </TouchableOpacity>
            <Text style={textStyle.title}>Welcome back</Text>
            <Text style={styles.text_2}>Login to your account</Text>
            <Text style={styles.text_3}>Mobile No.</Text>
            <TextInput
              style={styles.textInput}
              keyboardType="numeric"
              onChangeText={(text) => setMobile(text)}
              value={mobile}
              maxLength={10}
            />
            <TouchableOpacity
              style={buttonStyle.submit}
              activeOpacity={0.5}
              onPress={loginHandler}>
              <Text style={textStyle.button}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.view_1} onPress={navCreateAccount}>
              <Text style={styles.text_4}>Don't have an account? </Text>
              <Text style={styles.text_5}>Sign Up</Text>
            </TouchableOpacity>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
};

const styles = StyleSheet.create({
  text_2: {
    color: '#ACB1C0',
    fontSize: 17,
    fontFamily: 'Avenir',
    lineHeight: 22,
    fontWeight: '400',
    marginTop: 9,
    marginLeft: 30,
  },
  text_3: {
    color: '#ACB1C0',
    fontSize: 16,
    fontFamily: 'Avenir',
    lineHeight: 22,
    fontWeight: '400',
    marginTop: 98,
    marginLeft: 30,
  },
  text_4: {
    color: '#0A1F44',
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 20,
  },
  text_5: {
    color: '#EF4236',
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 20,
  },
  view_1: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  textInput: {
    height: 50,
    width: '82%',
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: '500',
    color: '#1E2432',
    marginTop: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    borderBottomColor: '#EAECEF',
    borderBottomWidth: 1,
  },
});

export default LoginAccount;
