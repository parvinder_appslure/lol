import AsyncStorage from '@react-native-community/async-storage';
import {PATH_URL, XApiKey} from './Config';
import ApiSauce from './ApiSauce';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoder';
import {Dimensions} from 'react-native';
import {latitudeDelta} from '../backend/Config';
const request = (path, json) => {
  return new Promise((resolve, reject) => {
    ApiSauce.post(path, json).then((response) => {
      // console.log(JSON.stringify(path, null, 2));
      // console.log(JSON.stringify(json, null, 2));
      if (response.ok) {
        // console.log(JSON.stringify(response.data, null, 2));
        resolve(response.data);
      } else {
        console.log(response.err);
        reject(response.err);
      }
    });
  });
};

export const SignUpApi = (json) => request(PATH_URL.SignUp, json);
export const SignInApi = (json) => request(PATH_URL.SignIn, json);
export const RegisterOtpApi = (json) => request(PATH_URL.RegisterOtp, json);
export const LoginOtpApi = (json) => request(PATH_URL.LoginOtp, json);
export const GetProfileApi = (json) => request(PATH_URL.GetProfile, json);
export const AmbulanceCategoryApi = (json) =>
  request(PATH_URL.AmbulanceCategory, json);
export const CouponApi = (json) => request(PATH_URL.Coupon, json);
export const BookingApi = (json) => request(PATH_URL.Booking, json);
export const EstimateApi = (json) => request(PATH_URL.Estimate, json);
export const BookingCheckApi = (json) => request(PATH_URL.BookingCheck, json);
export const BookingStatusForHomeApi = (json) =>
  request(PATH_URL.BookingStatusForHome, json);
export const BookingHistoryApi = (json) =>
  request(PATH_URL.BookingHistory, json);
export const ScheduleBookingHistoryApi = (json) =>
  request(PATH_URL.ScheduleBookingHistory, json);
export const CancelBookingApi = (json) => request(PATH_URL.CancelBooking, json);
export const CancelBookingUserApi = (json) =>
  request(PATH_URL.CancelBookingUser, json);
export const AddWalletApi = (json) => request(PATH_URL.AddWallet, json);

export const AsyncStorageSetUser = (user) =>
  AsyncStorage.setItem('user', JSON.stringify(user));
export const AsyncStorageGetUser = () => AsyncStorage.getItem('user');
export const AsyncStorageSetBookingId = (id) =>
  AsyncStorage.setItem('BookingId', id);
export const AsyncStorageGetBookingId = () => AsyncStorage.getItem('BookingId');
export const AsyncStorageClear = () => AsyncStorage.clear();
export const AsyncStorageSetTime = (time) => AsyncStorage.setItem('time', time);
export const AsyncStorageGetTime = () => AsyncStorage.getItem('time');
export const GeolocationInfo = () => {
  return new Promise((resolve, reject) => {
    Geolocation.getCurrentPosition((info) => {
      resolve(info);
    });
  });
};

export const GeocoderLocation = (lat, lng) => {
  return new Promise((resovle, reject) => {
    Geocoder.geocodePosition({lat: lat, lng: lng}).then((res) => resovle(res));
  });
};

export const AspectRatio = () =>
  Dimensions.get('window').width / Dimensions.get('window').height;
export const Height = Dimensions.get('window').height;
export const Width = Dimensions.get('window').width;
export const LongitudeDelta = () =>
  (latitudeDelta * Dimensions.get('window').width) /
  Dimensions.get('window').height;
export const LatitudeDelta = latitudeDelta;

export const formatAmount = (amount) =>
  `\u20B9 ${parseInt(amount)
    .toFixed(0)
    .replace(/(\d)(?=(\d\d)+\d$)/g, '$1,')}`;

export const formatNumber = (str) =>
  str.replace(/,/g, '').replace('\u20B9 ', '');

export const textInPrice = (price) => `\u20B9 ${price}`;

export const timeFormate_mmss = (time) => {
  let mm = Math.floor(time / 60);
  let ss = time % 60;
  mm = mm < 10 ? `0${mm}` : mm;
  ss = ss < 10 ? `0${ss}` : ss;
  return `${mm}:${ss}`;
};

// const fetchApi = (url, body) => {
//   return fetch(url, {
//     method: 'POST',
//     headers: {
//       'x-api-key': XApiKey,
//       'Content-Type': 'application/json',
//     },
//     body: JSON.stringify(body),
//   }).then((res) => res.json());
// };

// export const couponApi = (body) => {
//   return new Promise((resovle, reject) => {
//     fetchApi(
//       'http://68.183.85.226/projects/lol/webservices/coupan_verify',
//       body,
//     ).then((res) => resovle(res));
//   });
// };

// export const bookingApi = (body) => {
//   return new Promise((resovle, reject) => {
//     fetchApi(
//       'http://68.183.85.226/projects/lol/webservices/ambulence_request_sent',
//       body,
//     ).then((res) => resovle(res));
//   });
// };
