import {create} from 'apisauce';
import {ApiSauceJson} from './Config';
const ApiSauce = create(ApiSauceJson);
export default ApiSauce;
