const webClientId =
  '207538176069-cgih256ia35b2jlbcm1ta837l79ka22s.apps.googleusercontent.com';
const BASE_URL = 'http://139.59.67.166/lol/webservices/';
export const XApiKey = 'c3a3cf7c211b7c07b2495d8aef9761fc';
export const GOOGLE_MAPS_APIKEY = 'AIzaSyA_23OZbQeEKQeLfMBTJ6xd3-hCa33tK4A';
export const PATH_URL = {
  SignIn: '/signIn',
  SignUp: '/signUp',
  RegisterOtp: '/register_otp',
  LoginOtp: '/login_otp',
  GetProfile: '/getprofile',
  EditProfile: '/edit_user_profile',
  AmbulanceCategory: '/ambulence_category',
  Coupon: '/coupan_verify',
  Booking: './ambulence_request_sent',
  BookingCheck: './request_check_dynamics',
  BookingStatusForHome: './home_user',
  BookingHistory: './ambulance_booking_history',
  ScheduleBookingHistory: './pending_request_list',
  CancelBooking: './cancel_ambulance_booking',
  CancelBookingUser: './cancel_ambulance_booking_user',
  AddWallet: './Addwallet',
  Estimate: 'get_estimation',
};
export const GoogleSigninJson = {
  webClientId: webClientId,
  offlineAccess: true,
  hostedDomain: '',
  forceConsentPrompt: true,
};
export const ApiSauceJson = {
  baseURL: BASE_URL,
  headers: {
    'X-API-KEY': XApiKey,
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};
export const latitudeDelta = 0.0922;
