import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {LogBox, Platform} from 'react-native';
import Navigator from './src/components/Navigator';
import NetInfo from '@react-native-community/netinfo';
import DeviceInfo from 'react-native-device-info';
import * as actions from './src/redux/actions';
import store from './src/redux/store';
import {Provider} from 'react-redux';
import PushNotification from 'react-native-push-notification';
import Timer from './src/components/BookingTimer';

const App = () => {
  LogBox.ignoreLogs(['Warning:', 'Cannot update a component from inside']);
  LogBox.ignoreAllLogs(true);
  const pushNotificationMessage = ({title, message}) =>
    PushNotification.localNotification({
      title,
      message,
    });
  useEffect(() => {
    const netInfoSubscribe = NetInfo.addEventListener((state) =>
      store.dispatch(actions.SetNetInfo(state)),
    );
    PushNotification.configure({
      onRegister: ({token}) => {
        store.dispatch(
          actions.SetDeviceInfo({
            id: DeviceInfo.getDeviceId(),
            token: token.toString(),
            model: DeviceInfo.getModel(),
            os: Platform.OS,
          }),
        );
      },
      onNotification: (notification) => {
        // console.log('notification');
        // console.log(JSON.stringify(notification, null, 2));
        pushNotificationMessage(notification);
        const {data = {}} = notification;
        const {type = ''} = data;
        if (type === 'bookingcancel') {
          alert(notification.message);
        }
      },
      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION:', notification);

        // process the action
      },
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      senderID: '659247106062',
      popInitialNotification: true,
      requestPermissions: true,
    });

    PushNotification.localNotificationSchedule({
      //... You can use all the options from localNotifications
      message: 'My Notification Message', // (required)
      date: new Date(Date.now() + 60 * 1000), // in 60 secs
      allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
    });
    return () => {
      netInfoSubscribe();
    };
  }, []);
  return (
    <Provider store={store}>
      <Navigator></Navigator>
    </Provider>
  );
  //   return <Timer></Timer>;
};
export default App;
